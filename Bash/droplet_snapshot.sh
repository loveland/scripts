echo "Shell Script to snapshot digital ocean server based on droplet id"
read -p "Input Digital Ocean Token: " TOKEN
read -p "Input Droplet ID: " DROPLETID

curl -X POST -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer $TOKEN \
    -d '{"type":"snapshot","name":"
mtest_snap2
"}' \
    "https://api.digitalocean.com/v2/droplets/$DROPLETID/actions" 
