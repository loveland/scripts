#!/bin/bash
echo

MDM_UUID=$(profiles -Lv | awk '/attribute: name: MDM/,/attribute: profileUUID:/' | awk '/attribute: profileUUID:/ {print $NF}')
echo $MDM_UUID
if [ -z "$MDM_UUID" ]
then
echo "-MDM profile NOT found. Attempting to manage-"
jamf manage
else
echo "-MDM profile found. Removing MDM before attempting to manage-"
profiles -R -p "$MDM_UUID"
sleep 5
jamf manage
fi

echo
exit 0