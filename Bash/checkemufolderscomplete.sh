#!/bin/bash

# List of folders to check for
folders=("atari2600" "atari5200" "atari7800" "atari800" "atarilynx" "c64" "dreamcast" "gamegear" "gb" "gba" "gbc" "mastersystem" "megadrive" "msx" "n64" "neogeo" "nes" "ngp" "ngpc" "pcengine" "psp" "psx" "sega32x" "segacd" "sg-1000" "snes" "vectrex" "videopac" "virtualboy" "wonderswan" "wonderswancolor")

# Array to store missing folders
missing=()

# Loop through each folder
for folder in "${folders[@]}"; do
    # Check if folder exists
    if [ ! -d "$folder" ]; then
        missing+=("$folder")
    fi
done

# Check if any folders are missing
if [ ${#missing[@]} -eq 0 ]; then
    echo "All folders exist."
else
    echo "The following folders are missing:"
    for folder in "${missing[@]}"; do
        echo "$folder"
    done
fi

