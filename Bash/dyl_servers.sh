#!/bin/bash
echo
perl /usr/local/cyber/scripts/pbx_reload_all.pl
/usr/local/cyber/scripts/init.d/dyl_note restart 
/usr/local/cyber/scripts/init.d/dyl_dialctl_worker restart 
/usr/local/cyber/scripts/init.d/dyl_pbx_worker restart 
/usr/local/cyber/scripts/init.d/dyl_bulk restart 
screen -S apns_queue_consume -d -m /home/note/cyber/scripts/server/apns_queue_consume.pl
pause 5
/usr/local/cyber/scripts/init.d/dyl_dialctl restart 
/usr/local/cyber/scripts/init.d/dyl_pbx restart 
pause 5
ps -ef | grep note
echo 
echo "Successfully started dyl startup scripts..."
echo 
echo "done."
echo
