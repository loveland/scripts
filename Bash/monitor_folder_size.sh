#!/bin/bash

# Folder to monitor
FOLDER_TO_MONITOR="/Volumes/SF14TBHD/.Trashes/501/"
# Interval in seconds
INTERVAL=5

while true; do
    # Calculate folder size
    FOLDER_SIZE=$(du -sh "$FOLDER_TO_MONITOR" 2>/dev/null | awk '{print $1}')
    # Print the size on a single line
    echo -ne "Folder Size: $FOLDER_SIZE\r"
    # Wait for the specified interval
    sleep $INTERVAL
done

