#!/bin/bash

# Function to check if the Escape key is pressed
check_escape_key() {
    # Capture one keystroke without waiting for Enter (timeout 0.1 seconds)
    read -s -N 1 -t 0.1 key
    # Check if the key is Escape
    if [[ $key == $'\e' ]]; then
        exit 0
    fi
}

# Main loop
while :
do
    # Simulate pressing the right arrow key
    xdotool key Right

    # Check if the Escape key is pressed
    check_escape_key

    # Wait for 2 seconds before the next iteration
    sleep 2
done

