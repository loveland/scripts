#!/bin/bash
defaults write /Library/Preferences/com.apple.screensaver loginWindowIdleTime 60
defaults write /Library/Preferences/com.apple.screensaver MESSAGE "🍔 @ $(date +%r)"
defaults -currentHost write com.apple.screensaver modulePath -string "/System/Library/Screen\ Savers/FloatingMessage.saver" 

killall cfprefsd

osascript <<'END'
tell application "System Events"
set ss to screen saver "Message"
start ss
end tell
END

read VAR
if [[ -z $VAR ]]; then echo "Back from Lunch!"; fi
sleep 1

osascript <<'END'
tell application "System Events"
set ss to screen saver "Shell"
start ss
end tell
END