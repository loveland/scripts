#!/bin/bash

log_file="/var/log/system_fix.log"

echo "=== System Investigation and Fix Script ===" | tee -a $log_file
echo "Running on: $(hostname)" | tee -a $log_file
echo "Timestamp: $(date)" | tee -a $log_file

# Function to log status
log_status() {
    echo "[INFO] $1" | tee -a $log_file
}

# 1. Fix ZFS Modules Auto-Load
log_status "Checking and loading ZFS modules..."
if ! lsmod | grep -q zfs; then
    if modprobe zfs; then
        log_status "ZFS modules loaded successfully."
    else
        log_status "Failed to load ZFS modules. Check ZFS installation."
    fi
else
    log_status "ZFS modules already loaded."
fi

# 2. Check Hostname Configuration
log_status "Checking hostname configuration..."
if [ "$(hostname)" = "rp5" ]; then
    log_status "Setting FQDN (Fully Qualified Domain Name)..."
    sudo hostnamectl set-hostname rp5.localdomain
else
    log_status "Hostname is correctly configured."
fi

# 3. Investigate Memory Usage and OOM Killer
log_status "Investigating memory usage..."
free -h | tee -a $log_file
log_status "Top 5 memory-consuming processes:"
ps aux --sort=-%mem | head -n 6 | tee -a $log_file

log_status "Increasing swap space to avoid OOM kills..."
sudo fallocate -l 2G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

# 4. Fix PipeWire D-Bus Issues
log_status "Checking PipeWire status..."
if ! systemctl --user is-active pipewire; then
    log_status "Restarting PipeWire and D-Bus user services..."
    systemctl --user restart pipewire pipewire-pulse
    systemctl --user restart dbus.socket
else
    log_status "PipeWire is running correctly."
fi

# 5. Check and Fix D-Bus Issues
log_status "Verifying D-Bus socket..."
if ! systemctl is-active dbus.socket; then
    log_status "Restarting D-Bus socket..."
    sudo systemctl restart dbus.socket
else
    log_status "D-Bus socket is active."
fi

# 6. Fix Network Connectivity Issues
log_status "Checking network status..."
if ! systemctl is-active systemd-networkd; then
    log_status "Restarting network services..."
    sudo systemctl restart systemd-networkd
    sudo systemctl restart systemd-networkd-wait-online
else
    log_status "Network services are running."
fi

# 7. Investigate USB Errors
log_status "Checking USB devices..."
lsusb | tee -a $log_file
log_status "Restarting USB services..."
sudo modprobe -r usb-storage && sudo modprobe usb-storage

# 8. Fix XRDP Listening Issue
log_status "Checking XRDP status..."
if ! systemctl is-active xrdp; then
    log_status "Restarting XRDP service..."
    sudo systemctl restart xrdp
else
    log_status "XRDP is running correctly."
fi

# 9. General System Check and Reload Modules
log_status "Reloading system modules..."
sudo systemctl restart systemd-modules-load

# 10. Clean Up and Summary
log_status "Disabling unnecessary services to free memory..."
sudo systemctl disable gnome-control-center

log_status "System investigation and fixes completed."
echo "Check $log_file for detailed logs."

#!/bin/bash

# Ensure the script is run as root
if [[ $EUID -ne 0 ]]; then
    echo "Please run as root"
    exit 1
fi

echo "Updating system packages..."
# Update system packages to ensure all libraries are up-to-date
apt update && apt upgrade -y

echo "Reinstalling relevant packages..."
# Reinstall gnome-logs and relevant GTK libraries
apt install --reinstall gnome-logs libglib2.0-0 libgtk-3-0 -y

echo "Checking for missing dependencies..."
# Install missing dependencies if any are detected
apt install -f

echo "Fixing permissions for system logs..."
# Make sure the current user can access system logs
usermod -aG adm $SUDO_USER
usermod -aG systemd-journal $SUDO_USER

echo "Clearing gnome-logs cache..."
# Clear GNOME logs cache to remove any corrupted cached files
rm -rf ~/.cache/gnome-logs/*

echo "Checking GLib schemas..."
# Recompile GLib schemas in case of corruption
glib-compile-schemas /usr/share/glib-2.0/schemas/

echo "Fixing potential GTK theme issues..."
# Set default GTK theme (in case the current theme is incompatible or corrupted)
gsettings set org.gnome.desktop.interface gtk-theme "Adwaita"

echo "All steps completed. Please reboot your system and try running gnome-logs again."

exit 0