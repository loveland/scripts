chmod -R 755 /bin /boot /dev /etc/ /home /lib /lib64 \
/media /mnt /opt /run /sbin /srv /usr /var

chmod -R 777 /initrd.img /vmlinuz
chmod -R 1777 /tmp
chmod -R 555 /sys
chmod -R 555 /proc
chmod -R 700 /root

cd /home/
for p in $(ll); do chown $p.$p -R /home/$p; done
find . -type d -print0 | xargs -0 chmod 755
find . -type f -print0 | xargs -0 chmod 644
chmod 700 *

