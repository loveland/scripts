#!/bin/bash

# Prompt the user for the .dmg file path
echo "Please enter the path to the .dmg file:"
read dmg_path

# Check if the .dmg file exists
if [ ! -f "$dmg_path" ]; then
    echo "Error: The file $dmg_path does not exist. Please try again."
    exit 1
fi

# Prompt the user for the output .img file path
echo "Please enter the output path and name for the .img file (e.g., /path/to/output.img):"
read img_path

# Convert the .dmg to .img using hdiutil
hdiutil convert "$dmg_path" -format UDRW -o "$img_path"

# Provide feedback
if [ $? -eq 0 ]; then
    echo "Conversion successful! The .img file is located at: $img_path"
else
    echo "Error: Conversion failed."
    exit 1
fi

