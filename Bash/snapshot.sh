#!/bin/bash
#
#/usr/local/bin/rsnapshot.sh
#
#Below will maintain a tree of snapshots of your files. In other words, a directory with date-ordered copies of the files. The copies are made using hardlinks, which means that only files that did change will occupy space. Generally speaking, this is the idea behind Apple's TimeMachine.
#keeps an up-to-date full backup $SNAP/latest and in case a certain number of files has changed since the last full backup, it creates a snapshot $SNAP/$DATETAG of the current full-backup utilizing cp -al to hardlink unchanged files
#
#Restore a backup
#If you wish to restore a backup, use the same rsync command that was executed but with the source and destination reversed.
#
# config vars

SRC="/"
SNAP="root@storage.dyl.com:/backup/snapshots/" #dont forget trailing slash
OPTS="-rltgoi --delay-updates --delete --chmod=a-w"
MINCHANGES=20

# run this process with real low priority
#pid=$(ps -ax | awk '/snapshot.sh/{print $1}')

nice -n 3 $$
renice +12 -p $$

# sync

rsync $OPTS $SRC $SNAP/latest >> $SNAPrsync.log

# check if enough has changed and if so
# make a hardlinked copy named as the date

COUNT=$( wc -l $SNAPrsync.log|cut -d" " -f1 )
if [[ $COUNT -gt $MINCHANGES ]]; then
	DATETAG=$(date +%Y-%m-%d)
	if [ ! -e $SNAP/$DATETAG ]; then
	    cp -al $SNAP/latest $SNAP/$DATETAG
	    chmod u+w $SNAP/$DATETAG
	    mv $SNAP/rsync.log $SNAP/$DATETAG
	    chmod u-w $SNAP/$DATETAG
	 fi
fi

#Full system backup one liner excluding some dirs
#
# rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / /path/to/backup/folder
#
#If you plan on backing up your system somewhere other than /mnt or /media, do not forget to add it to the list of exclude patterns to avoid an infinite loop.
#If there are any bind mounts in the system, they should be excluded as well so that the bind mounted contents is copied only once.
#If you use a swap file, make sure to exclude it as well.
#Consider if you want to backup the /home/ folder. If it contains your data it might be considerably larger than the system. Otherwise consider excluding unimportant subdirectories such as /home/*/.thumbnails/*, /home/*/.cache/mozilla/*, /home/*/.cache/chromium/*, and /home/*/.local/share/Trash/*, depending on software installed on the system. If GVFS is installed, /home/*/.gvfs must be excluded to prevent rsync errors.
#
#
#For successful cloning at the file system level, some additional options need to be provided
#
#rsync -qaHAXS SOURCE_DIR DESTINATION_DIR
#
#-H, --hard-links      preserve hard links
#-A, --acls            preserve ACLs (implies -p)
#-X, --xattrs          preserve extended attributes
#-S, --sparse          handle sparse files efficiently
#