#!/bin/bash

# Function to check if a service is running and stop it
stop_service() {
    local service_name="$1"
    if systemctl is-active --quiet "$service_name"; then
        echo "Stopping and disabling $service_name..."
        sudo systemctl stop "$service_name"
        sudo systemctl disable "$service_name"
    else
        echo "$service_name is not running or already disabled."
    fi
}

# Define services to stop (excluding ssh and vncserver)
services=(
    "rpcbind"         # Port 111
    "imap"            # Port 143
    "uucp"            # Port 540
    "ingreslock"      # Port 1524
    "callbook"        # Port 2000
    "remote-desktop"  # Port 3389 (if using ms-term-serv equivalent)
    "irc"             # Port 6667
    "trinoo"          # Port 27665 (if exists for DDoS testing, replace with correct service name)
    "sometimes-rpc"   # Port 32771 and 32774, adjust as per specific service names if they exist
)

# Loop through and stop each service
for service in "${services[@]}"; do
    stop_service "$service"
done

echo "All specified services stopped and disabled, excluding SSH and VNC."

