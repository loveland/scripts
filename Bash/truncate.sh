find . -type f | while read -r file; do
    filename=$(basename "$file")
    dirname=$(dirname "$file")
    if [[ ${#filename} -gt 255 ]]; then
        newname=$(echo "$filename" | cut -c 1-255)
        mv "$file" "$dirname/$newname"
    fi
done

