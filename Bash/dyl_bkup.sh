echo "DYL Backup and Restore #loveland" | lolcat
echo
echo "This utility will backup the most relevant files on employee mac workstations which includes:"
echo
echo "Backed up locations:"
echo "--------------------"
echo "~/Desktop"
echo "~/Documents"
echo "~/Downloads"
echo "~/Music"
echo "~/Videos"
echo "~/Pictures"
echo "~/Dropbox"
echo "& Mac Notes"
echo
echo "Upon running this application again, this program will then determine if there is your backup file on a new desktop and then"
echo "extract the contents from your backup into the same locations following a system migration for convienience."
echo
echo

filename=$(hostname)_bkup.zip
testfor="_bkup.zip"

if [[ $filename =~ $testfor ]]; then
	
	actualfile=$(ls *_bkup.zip)
	dirname=$(echo $actualfile | rev | cut -c 5- | rev)

	echo "Restoring files from backup..." | lolcat 
	
	cd ~/Desktop
	open $actualfile -a Archive\ Utility.app
	cd $dirname/Desktop

	mv * ~/Desktop
	
	cd ..
	cd Documents
	mv * ~/Documents
	
	cd ..
	cd Downloads
	mv * ~/Downloads
	
	cd ..
	cd Music
	mv * ~/Music
	
	cd ..
	cd Videos
	mv * ~/Videos
	
	cd ..
	cd Pictures
	mv * ~/Pictures
	
	cd ..
	mv Dropbox ~/Dropbox
	mv group.com.apple.notes ~/Library/Group\ Containers/group.com.apple.notes/
	mv Notes ~/Library/Containers/com.apple.Notes/Data/Library/Notes/

else

	echo "Backing up files to Desktop." | lolcat 
	brew install p7zip
	7z a $HOME/Desktop/"$filename" $HOME/Desktop $HOME/Documents $HOME/Downloads $HOME/Music $HOME/Videos $HOME/Pictures $HOME/Dropbox ~/Library/Group\ Containers/group.com.apple.notes/ ~/Library/Containers/com.apple.Notes/Data/Library/Notes/
fi

echo "Complete! Press any key to close this window and exit the terminal or type exit to exit to shell:" | lolcat

read -p "" exitt

if [ "$exitt" == "exit" ]; then
	
	exit 1;

elif [ "$exitt" != "exit" ]; then

	clear
	echo "Buh Bye!"
	sleep 1
	superexit=$(ps ax | awk '! /awk/ && /Terminal/ { print $1}')
	kill -9 $superexit

fi
