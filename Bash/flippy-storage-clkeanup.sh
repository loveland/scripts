#!/bin/bash

# Clean up files that are no longer needed - meant to be ran daily
find /home/flippy/releases/*/saved_data/ -mindepth 1 -mtime +90 -name *.png -delete
find /home/flippy/.ros/log/ -mindepth 1 -mtime +14 -type f -delete
find /home/flippy/.ros/rosbag_data/data_dumps -mindepth 1 -mtime +28 -type f -delete