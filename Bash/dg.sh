#!/bin/bash

# Run the program using nohup
nohup sudo /Applications/dupeguru.app/Contents/MacOS/dupeguru > /dev/null 2>&1 &

# Get the PID of the last background command
pid=$!

# Give it a second to start
sleep 2

# Check if the process is suspended
if ps -o state= -p $pid | grep -q 'T'; then
    echo "Process is suspended, attempting to resume..."
    kill -CONT $pid
    echo "Process resumed."
fi

