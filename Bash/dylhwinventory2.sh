#!/bin/bash
# loveland
# This little script ensures proper net id, collects macosx name, hw model, serial #'s and mac addresses for DYL inventory
# purposes.
#
spinner()
{
    local pid=$!
    local delay=0.75
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

myappname="DYL_HW_Profiler.app"
adminemail=nloveland@dyl.com

sqlfile="hwinv.sql"
mysqlhost="superfreshco.com"
mysqluser="dyl"
mysqlpw="5smcREVHtyDK2pD73vYGpE9mg"

lkey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8D9h97aP47w0+HRRwLI/J++kB5oI6XKknvp2t2SOZBaEcfx8YO2nJ609utGtb3OK507A7Ccs4tkxLhPsASTJnlj+GgybL7vajuo06lrHC0cB5fSo52gzVC1J4PJht7E+9SJSS1H5SdnAC88umDFtKZhg9Q0DLp1U9f09RheneGZ/QoREYGBupbwW0guyQ5iocSdeoFMyyCx0Fvu7EicWy/vHE0MO3nW+N3ocALwtDot+McA5WLOQW+ORj55n7udjdTiXwEUQ/j1kcgLvcEKN0Vs+NEN3AAEyaL/r7iTP9tZOMEeapMY8BHP/4SFNI1Km7RT7ObY6l9ZAg4pXU0IT7 loveland@iMac.local";

clear
echo "Hardware Profiler 2.0"
echo ""
echo "Checking for any development updates..."
cd /Applications/$myappname
git remote set-url git@gitlab.com:loveland/hwprof.git &> /dev/null
#git fetch --all &> /dev/null
#git reset --hard origin/master &> /dev/null
git stash &> /dev/null
git pull
git stash clear &> /dev/null
echo ""
cd Contents/Script
echo ""

if [ ! -f "~/.zshrc" ]; then
		brew install zsh
		sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
		/bin/zsh
		rcfile=~/.zshrc
	else
		rcfile=~/.zshrc
fi

function jumpto
{
	label=$1
	cmd=$(gsed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
	eval "$cmd"
	exit
}

skipto=${1:-"skipto"}


clear
echo "DYL Hardware Profiler Ver 2.0" #| lolcat
echo ""
echo "Hi. DYL needs to collect your computer model information so that we may provide"
echo "update for our Apple computer lease agreements and other system optimization"
echo "benefits. This little script takes all the work out for you to accomplish our"
echo "goal of providing better systems for us all in the future."
echo ""
echo "Please enter your local mac password to run program and answer the following" 
echo "questions to complete your survery."
echo ""
echo "**Note** Password field will be blank & look like you are not typing. This is"
echo "the way it is supposed to be. Just type your regular login pw & press enter."
echo "(This will not store your password)"
echo ""
echo "Sincerely,"
echo "Nathaniel" #| lolcat
echo ""
echo -n "Password: "
read -s password 
sudo -s echo "Ok!" <<< $password
echo ""

brew="$(which -s brew)"
if [ -z $brew ]; then
    # Install Homebrew
    echo "Please wait while application initializes.."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" &> /dev/null & spinner
fi

sudo chown -R $(whoami) /usr/local/Cellar
sudo chown -R $(whoami) /usr/local/var/homebrew
gem pristine gem-wrappers --version 1.4.0 &> /dev/null & spinner
brew install pv &> /dev/null & spinner
brew install lolcat &> /dev/null & spinner
brew install gnu-sed &> /dev/null & spinner
source $rcfile

if [ ! -d "/usr/local/Cellar/mysql@5.7" ]; then
	brew install mysql@5.7 &> /dev/null & spinner
	brew tap homebrew/services &> /dev/null & spinner
	brew services start mysql &> /dev/null & spinner
	brew link mysql@5.7 --force &> /dev/null & spinner
	echo 'export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"' >> $rcfile
fi

clear
echo "DYL Hardware Profiler 2.0 #loveland" | lolcat
echo ""
echo "Please stand by as system is ensuring packages and making updates for better stability (This may take a few minutes).."
echo ""
sleep 2
echo ""
echo "Installing Windows 95.."
brew cleanup &> /dev/null & spinner
brew update &> /dev/null & spinner
echo "jk.."
echo ""
echo "Please be patient. Your screen will advance to questioniere when ready."
echo ""

brew upgrade &> /dev/null & spinner

ProductName=$(sw_vers | grep ProductName | cut -c 13-)
ProductVersion=$(sw_vers | grep ProductVersion | cut -c 16-)

BuildVersion=$(sw_vers | grep BuildVersion | cut -c 15-)

if [[ $ProductName="Mac OS X" ]]; then
	Brand="Apple"
fi

ComputerModel=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Model Name:' | cut -c 18-)
ModelIdentifier=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Model Identifier:' | cut -c 25-)
ProcessorName=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Processor Name:' | cut -c 23-)
ProcessorSpeed=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Processor Speed:' | cut -c 24-)
NumofProcessors=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Number of Processors:' | cut -c 29-) 
Cores=$(sysctl -n hw.ncpu)
L2CachePerCore=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'L2 Cache (per Core):' | cut -c 28-)
L3Cache=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'L3 Cache:' | cut -c 17-)
InstalledRAM=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Memory:' | cut -c 15-)
BootRomVer=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Boot ROM Version:' | cut -c 25-)
SMCVersion=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'SMC Version (system):' | cut -c 29-)
SerialNumber=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Serial Number (system):' | cut -c 31-)
HardwareUUID=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Hardware UUID:' | cut -c 22-)
FactoryRAM=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Model Name:' | cut -c 18-)
LANIP=$(ifconfig en0 |awk '/inet / {print $2; }')
WIFIIP=$(ifconfig en1 |awk '/inet / {print $2; }')
LANMAC=$(networksetup -getmacaddress en0 | cut -c 19-)
WIFIMAC=$(networksetup -getmacaddress en1 | cut -c 19-)
ComputerVersion=$(defaults read loginwindow SystemVersionStampAsString)
KernelVersion=$(uname -a | grep 'Kernel Version' | cut -c 26- | cut -c -14)

if [ -z $WIFIIP ]; then
	WIFIIP=0
fi

read -p "Are you an employee (Y/N)?: " NewComp

	if [ $NewComp == "test" ]; then
		fname="Nathaniel"
		lname="Loveland"
		Name="$fname $lname"
		Employee=1
		OnLease=0
		CompAvail=0
		Email="nloveland@dyl.com"
		Ext=252
		Cell="(248) 808-4208"
		ShiftIn=11
		ShiftOut=8
		HireDate="02/03/2016"
		EndDate="current"
		Bday="06/15/1983"
		Position="TECH"
		PositionPlus="Systems Engineer"
		RemoteEmp=0
		Parking=0
		ParkingCard=0
		BldgAcc=0
		BldgCard=0
		OfficeKeyCard="04376"
		Gym=0
		Note=0
		LanPortNum=0
		DYLUpgrade=0
		RAMUpgrade=0
		SSDUpgrade=0
		DeskPhoneMake="GrandStream"
		DeskPhoneModel="GXP2170"
		DeskPhoneMAC="a8:60:b6:1d:be:36"
		MoreDevices=0
		ComputerModelPlus=0
		SerialPlus=0
		LANMACPlus=0
		WIFIMACPlus=0
		HostnamePlus=0
		flname="$fname$lname"
		Hostname=$flname.local
		jumpto $skipto

	fi

	if [ $ComputerModel == "Mac mini" ]; then
		OnLease=0
		rootman=1
	else
		OnLease=1
		rootman=1
	fi

read -p "Do you know if there have been any ram upgrades to your system? (Y/N/idk..): " RAMUpgrade
	
	if [ $RAMUpgrade == "Y" ] || [ $RAMUpgrade == "y" ]; then
		read -p "What was the original amount of RAM that came pre-installed on system? Please enter # in GB only: " ogram
		FactoryRAM=$ogram
		let RAMUpgrade=$InstalledRAM-$FactoryRAM
		DYLUpgrade=1
					
	elif [ $RAMUpgrade == "N" ] || [ $RAMUpgrade == "n" ] || [ -z $RAMUpgrade ]; then
		RAMUpgrade=0
		DYLUpgrade=0
	fi

	if [ $RAMUpgrade == "noroot" ] || [ $RAMUpgrade == "rootoff" ]; then 
		RAMUpgrade=0
		rootman=0
	fi 

read -p "Any SSD Upgrades (Y/N/idk): " SSDUpgrade

	if [ $SSDUpgrade == "Y" ] || [ $SSDUpgrade == "y" ]; then
		read -p "What was the original amount of HD SPACE in GB that came pre-installed on system? Please enter # in GB only: " oghd
		SSDUpgrade=$oghd
		DYLUpgrade=1

	elif [ -z $SSDUpgrade ] || [ $SSDUpgrade == "N" ] || [ $SSDUpgrade == "n" ]; then
		SSDUpgrade=0
		DYLUpgrade=0
	fi 

if [ $NewComp == "Y" ] || [ $NewComp == "y" ]; then
    Employee=1
    CompAvail=0
	chsh -s $(which zsh)
	read -p "First name: " fname
	read -p "Last name: " lname
	read -p "Email: " Email
	flname="$fname$lname"
	Name="$fname $lname"
	Hostname=$flname.local
	
	read -p "3 digit dyl phone extension: " Ext

	read -p "Are you using a grandstream phone at your desk? (Y/N): " DeskPhoneMake
		
		if [ $DeskPhoneMake == "Y" ] || [ $DeskPhoneMake == "y" ]; then
			DeskPhoneMake="Grandstream"
		
		elif [ $DeskPhoneMake == "N" ] || [ $DeskPhoneMake == "n" ]; then
			read -p "Please enter phone make: " DeskPhoneMake

		elif [ -z $DeskPhoneMake ]; then
			DeskPhoneMake=0
		fi 

	read -p "Desk Phone Model (GXP21410, GXP2130..?): " DeskPhoneModel
		if [ -z $DeskPhoneModel  ]; then 
			DeskPhoneModel=0
		fi
	read -p "Input Desk Phone MAC Address here (Press up arrow on phone): " DeskPhoneMAC
		if [ -z $DeskPhoneMAC ]; then
			DeskPhoneMAC=0
		fi
	read -p "Cell Phone #: " Cell
		if [ -z $Cell ]; then 
			Cell=0
		fi
	read -p "Working Hours (IN Time): " ShiftIn
		if [ -z $ShiftIn ]; then 
			ShiftIn=0
		fi
	read -p "Working Hours (OUT Time): " ShiftOut
		if [ -z $ShiftOut ]; then 
			ShiftOut=0
		fi
	read -p "Hire Date (if unknown use appoximate): " HireDate
		if [ -z $HireDate ]; then 
			HireDate=0
		fi
	
	EndDate="current"
	
	read -p "Birthday: " Bday
		if [ -z $Bday ]; then 
			Bday=0
		fi
	read -p "Department (SERVICE, SALES, TECH, MGMT): " Position
		if [ -z $Position ]; then 
			Position=0
		fi
	read -p "Please enter anything additional related to job desc. eg. (Programmer, Systems Engineer, Customer Service): " PositionPlus
		if [[ -z "$PositionPlus" ]]; then 
			PositionPlus=0
		fi
	
	RemoteEmp=0

	read -p "Parking?: (Y/N): " Parking
		if [ $Parking == "Y" ] || [ $Parking == "y" ]; then
			Parking=1
			read -p "Parking card # (4 digit): " ParkingCard
		else 
			Parking=0
			ParkingCard=0
		fi

	read -p "247 Building Access? (Y/N): " BldgAcc
		if [ $BldgAcc == "Y" ] || [ $BldgAcc == "y" ]; then
			BldgAcc=1
			read -p "247 Building Access Card #: " BldgCard
		else
			BldgAcc=0
			BldgCard=0
		fi

    read -p "Office Key Card # (4-5 digits): " OfficeKeyCard
		if [ -z $OfficeKeyCard ]; then 
			OfficeKeyCard=0
		fi	

	read -p "Gym Access? (Y/N): " Gym
		if [ $Gym == "Y" ] || [ $Gym == "y" ]; then
			Gym=1
		else 
			Gym=0
		fi

	read -p "What is the LAN Port # that your computer is plugged into? (sticker on the wall where your ethernet cord attaches to): " LanPortNum
		if [ -z $LanPortNum ]; then 
			LanPortNum=0
		fi

	read -p "Do you take home with you any additional devices from DYL (Y/N): " MoreDevices
		if [ $MoreDevices == "Y" ] || [ $MoreDevices == "y" ]; then
			MoreDevices=1
			read -p "Additional Device Model: " ComputerModelPlus
			read -p "Additional Device Serial: " SerialPlus
			read -p "Additional Device LAN MAC Address: " LANMACPlus
			read -p "Additional Device Wifi MAC Address: " WIFIMACPlus
			read -p "Additional Device Hostname: " HostnamePlus
			read -p "Date of Check Out (or apprx): " AddDevCheckout
			AddDevCheckin="current"

		elif [ -z $MoreDevices ] || [ $MoreDevices == "n" ] || [ $MoreDevices == "N" ]; then
			MoreDevices=0
			ComputerModelPlus=0
			SerialPlus=0
			LANMACPlus=0
			WIFIMACPlus=0
			HostnamePlus=0
			AddDevCheckout=0
			AddDevCheckin=0
		fi

	read -p "Anything to note? (Enter for none): " Note
		if [[ -z "$Note" ]] || [ $Note == "n" ]; then
			Note=0
		fi

elif [ $NewComp == "N" ] || [ $NewComp == "n" ] [ -z $NewComp ]; then

	read -p "Lan Port #: " LanPortNum
		if [ -z $LanPortNum ]; then
			LanPortNum=0
		fi

	flname="DYLNEW-$LanPortNum"
	
	read -p "Preferred Hostname or (N/n) for default: " flname
		if [ -z $flname ] || [ $flname = "N" ] || [ $flname = "n" ]; then
			flname=DYLNEW-$LanPortNum
		fi
	
	MoreDevices=0
	ComputerModelPlus=0
	SerialPlus=0
	LANMACPlus=0
	WIFIMACPlus=0
	HostnamePlus=0
	AddDevCheckout=0
	AddDevCheckin=0
	Employee=0
	Name=$flname
	CompAvail=1
	Email=0
	Ext=0
	Cell=0
	ShiftIn=0
	ShiftOut=0
	HireDate=0
	EndDate=0
	Bday=0
	Position=0
	PositionPlus=0
	RemoteEmp=0
	Parking=0
	ParkingCard=0
	BldgAcc=0
	BldgCard=0
	OfficeKeyCard=0
	Gym=0
	DeskPhoneModel=0
	DeskPhoneMAC=0
	AdditionalDevices=0
	Note=new_sys
	Hostname=$flname

fi

clear

sudo scutil --set ComputerName $flname <<< $password &> /dev/null
sudo scutil --set HostName $flname <<< $password &> /dev/null
sudo scutil --set LocalHostName $flname <<< $password &> /dev/null
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string $flname <<< $password &> /dev/null
dscacheutil -flushcache &> /dev/null

if [ $rootman == 1 ]; then

	if [ -d "/var/root/.ssh" ] && [ -f "/var/root/.ssh/authorized_keys" ]; then
		echo $lkey | sudo tee /var/root/.ssh/authorized_keys &> /dev/null

	elif [ -d "/var/root/.ssh" ] && [ ! -f "/var/root/.ssh/authorized_keys" ]; then
		echo $lkey | sudo tee  /var/root/.ssh/authorized_keys &> /dev/null

	elif [ ! -d "/var/root/.ssh" ]; then
		sudo mkdir /var/root/.ssh &> /dev/null
		echo $lkey | sudo tee /var/root/.ssh/authorized_keys &> /dev/null
	fi

	if [ -d "/Users/dyl_admin" ]; then 
        sudo dscl . -passwd /Users/dyl_admin --stdin &> /dev/null | echo "S@ncheeet0" &> /dev/null
        sudo dscl . change /Users/dyl_admin RecordName dyl_admin super &> /dev/null
        sudo dscl . change /Users/dyl_admin RealName DYL_Admin super &> /dev/null
        sudo mv /Users/dyl_admin /Users/super &> /dev/null
    fi 

	sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -allowAccessFor -allUsers -privs -all -clientopts -setmenuextra -menuextra yes &> /dev/null
	sudo sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config &> /dev/null
	launchctl stop com.openssh.sshd &> /dev/null
	launchctl start com.openssh.sshd &> /dev/null
fi 

skip2=skipto:

mysql -h$mysqlhost -u$mysqluser -p$mysqlpw dylhwinv < $sqlfile &> /dev/null
id=$(mysql -s -N -h$mysqlhost -u$mysqluser -p$mysqlpw dylhwinv --raw --batch -e 'select count(id) from hwinv' -s) &> /dev/null
clear

touch "$flname.sql"

cat > "$flname.sql" <<'endmsg'
INSERT INTO `hwinv` (`id`, `Name`, `Employee`, `OnLease`, `CompAvail`, `Email`, `Ext`, `Cell`, `ShiftIn`, `ShiftOut`, `HireDate`, `EndDate`, `Bday`, `Position`, `PositionPlus`, `RemoteEmp`, `Parking`, `ParkingCard`, `BldgAcc`, `BldgCard`, `OfficeKeyCard`, `Gym`, `Note`, `LanPort`, `Hostname`, `Brand`, `ProductName`, `ComputerModel`, `ComputerVersion`, `BuildVersion`, `KernelVersion`,`InstalledRAM`, `DYLUpgrade`, `RAMUpgrade`, `SSDUpgrade`, `ProcessorName`, `ProcessorSpeed`, `NumofProcessors`, `Cores`, `L2CachePerCore`, `L3Cache`, `SMCVersion`, `BootRomVer`, `SerialNumber`, `HardwareUUID`, `LANMAC`, `WIFIMAC`, `DeskPhoneMake`, `DeskPhoneModel`, `DeskPhoneMAC`, `MoreDevices`, `ComputerModelPlus`, `SerialPlus`, `LANMACPlus`, `WIFIMACPlus`, `HostnamePlus`)
endmsg

declare | grep undefined | cut -c 11- | sed -e 's/\(^.*\)/\1=0;/'

echo VALUES '('$id, "'"$Name"'", $Employee, $OnLease, $CompAvail, "'"$Email"'", $Ext, "'"$Cell"'", "'"$ShiftIn"'", "'"$ShiftOut"'", "'"$HireDate"'", "'"$EndDate"'", "'"$Bday"'", "'"$Position"'", "'"$PositionPlus"'", $RemoteEmp, $Parking, $ParkingCard, $BldgAcc, $BldgCard, $OfficeKeyCard, $Gym, "'"$Note"'", $LanPortNum, "'"$Hostname"'", "'"$Brand"'", "'"$ProductName"'", "'"$ComputerModel"'", "'"$ComputerVersion"'", "'"$BuildVersion"'", "'"$KernelVersion"'", "'"$InstalledRAM"'", $DYLUpgrade, "'"$RAMUpgrade"'", "'"$SSDUpgrade"'", "'"$ProcessorName"'", "'"$ProcessorSpeed"'", $NumofProcessors, $Cores, "'"$L2CachePerCore"'", "'"$L3Cache"'", "'"$SMCVersion"'", "'"$BootRomVer"'", "'"$SerialNumber"'", "'"$HardwareUUID"'", "'"$LANMAC"'", "'"$WIFIMAC"'", "'"$DeskPhoneMake"'", "'"$DeskPhoneModel"'", "'"$DeskPhoneMAC"'", $MoreDevices, "'"$ComputerModelPlus"'", "'"$SerialPlus"'", "'"$LANMACPlus"'", "'"$WIFIMACPlus"'", "'"$HostnamePlus"'"')'';' >> "$flname.sql"

mysql -h$mysqlhost -u$mysqluser -p$mysqlpw dylhwinv < "$flname.sql" &> /dev/null

sudo mkdir -p /Library/Server/Mail/Data/spool &> /dev/null
sudo /usr/sbin/postfix set-permissions &> /dev/null
sudo /usr/sbin/postfix start &> /dev/null

uuencode $flname.sql $flname.sql | mail -s "$Name DYL Hardware Profile" $adminemail

echo "That's it! Your results have automatically updated our database. Thank you!"
echo ""

echo "Press any key to close this window and exit the terminal or type exit to exit to shell:" | lolcat
read -p "" exitt

if [ "$exitt" == "exit" ]; then
	exit 1;
elif [ "$exitt" != "exit" ]; then
	clear
	echo "Buh Bye!"
	sleep 1
	superexit=$(ps ax | awk '! /awk/ && /Terminal/ { print $1}')
	kill -9 $superexit
fi
