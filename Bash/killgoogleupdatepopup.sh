#/bin/bash
#
# This will prevent annoying google update popups. if you want to update google chrome you can do so by downloading google chrome whenever you'd like.. and rerunning this.. 
#
# Note: You can run this with Google Chrome Open or closed. However, once it's run, if you close or re-open chrome again it will popup one more time. This is a new check Chrome has on thier end. Just click cancel..one last time.. And you shouldn't see it again after that. 

defaults write com.google.Keystone.Agent checkInterval 0

~/Library/Google/GoogleSoftwareUpdate/GoogleSoftwareUpdate.bundle/Contents/Resources/ksinstall --uninstall
sudo /Library/Google/GoogleSoftwareUpdate/GoogleSoftwareUpdate.bundle/Contents/Resources/ksinstall --uninstall

rm -rf ~/Library/Google/GoogleSoftwareUpdate
sudo rm -rf /Library/Google/GoogleSoftwareUpdate

rm ~/Library/Preferences/com.google.Keystone.Agent.plist

rm ~/Library/Caches/com.google.Keystone.Agent
rm ~/Library/LaunchAgents/com.google.Keystone.agent.plist

clear

echo "Congratulations. Google Update Popup Successfully Removed. Please contact Nathaniel if it comes back. You may now close this window."
echo