#/bin/bash
#loveland
clear
echo "*-----------------------------------*" | lolcat
echo "|- - - - - - - - - - - - - - - - - -|" | lolcat
echo "|     SuperFresh Clone Utility      |" | lolcat
echo "|            (fast dd)              |" | lolcat
echo "|            #LOVELAND              |" | lolcat
echo "|- - - - - - - - - - - - - - - - - -|" | lolcat
echo "*-----------------------------------*" | lolcat
echo

echo "Available Disks:" | lolcat
echo "----------------" | lolcat
echo
diskutil list | grep /dev/disk
echo

echo "Clone FROM Path (drag file or input iso etc)"| lolcat
read -p "Img path: " ptf
echo "Clone TO Path: (input only disk #. e.g. 2, 3.. )" | lolcat
read -p "To path: " dpath
dpath="/dev/disk$dpath"
echo

sudo -i
echo
echo "Choose your clone tool" | lolcat
echo "----------------------" | lolcat
echo "1 = pv tool. (Automatically chooses best disk speed performance. Works on mac and linux)"
echo "2 = dd tool. (command line only tool. Useful for cloning larger drive to smaller drive when space isn't all used with option conv=sparse. Option to run disk benchmarking tool for optimal perfromance. Works on mac and linux)"
echo "3 = dd tool with status window. (Option to run disk benchmarking tool for optimal perfromance. Progress bar status window. Works on linux)"
echo

read -p "Choose a #: " tool

if [[ $tool == 1 ]]; then
	pv < $ptf > $dpath | lolcat
else
	bmtool
	read -p "Input desired dd bs variable or press enter to use default 128M: " bs
		if [[ $bs == "" ]]; then
			bs="128M"
		fi

elif [[ $tool == 2 ]]; then
	dd if=$ptf of=$dpath conv=sparse bs=$bs

elif [[ $tool == 3 ]]; then
	bmtool
	(pv -n $ptf | dd of=$dpath bs=$bs conv=notrunc,noerror,sparse) 2>&1 | dialog --gauge "Running dd command (cloning), please wait..." 10 70 0
fi

function bmtool () {
	echo "SFCO Benchmark Util" | lolcat
	echo "This utility will test read and write speeds based on user disk input"
	echo

	disksavail=$(diskutil list | grep /dev/disk)
	IFS=': ' read -r -a array <<< "$disksavail"
	echo

	echo "Available Disks:" | lolcat
	echo "----------------" | lolcat
	echo "${array[@]}"
	echo

	read -p "Test local disk speed? (Y/N): " loctest

	if [[ $loctest == "Y" ]]; then
		echo $ptf | lolcat
		echo "Local Disk Read Speed:"
		readspeedfrom=$(sudo dd if=tstfile bs=1024k of=/dev/null count=1024)
		echo "Local Disk Write Speed:"
		writespeedfrom=$(sudo time dd if=/dev/zero bs=1024k of=tstfile count=1024)
		echo
		read -p "Additional disk to benchmark? (Y/N): " dpath
			if [[ $dpath == "N" ]]; then
				echo "Done!"
				exit
			fi
	else
		read -p "Input Disk to Benchmark: " dpath
	fi

	echo
	echo "Benchmarking $dpath.." | lolcat
	echo
	echo "Read Speed:"
	readspeedto=$(sudo dd if=tstfile bs=1024k of=$dpath count=1024)
	echo "Write Speed:"
	writespeedto=$(sudo time dd if=$dpath bs=1024k of=tstfile count=1024)
	echo
	echo "Done!"
}
exit
