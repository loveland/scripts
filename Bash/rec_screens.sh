#!/bin/bash
#As note on CTL2 run this in order to initiate startup scripts after install for call recording. 
echo
echo "CTL2"
echo "Start rec_gearman_worker and gearman"
screen -ls | grep Detached | cut -d. -f1 | awk '{print $1}' | xargs kill #kill all detached screen sessions
screen -S rec_gearman_worker -d -m /home/note/cyber/scripts/server/rec_gearman_worker.pl
pause 8
screen -S rec_gearman -d -m /home/note/cyber/scripts/server/rec_gearman.pl
pause 8
