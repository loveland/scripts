#!/bin/bash

# Define associative array to map file extensions to folder names
declare -A extensions=(
    ["atari2600"]=".bin"
    ["atari5200"]=".bin"
    ["atari7800"]=".a78"
    ["atari800"]=".atr .xex .rom"
    ["atarilynx"]=".lnx"
    ["c64"]=".d64 .t64 .crt"
    ["dreamcast"]=".cdi .gdi"
    ["gamegear"]=".gg"
    ["gb"]=".gb"
    ["gba"]=".gba"
    ["gbc"]=".gbc"
    ["mastersystem"]=".sms"
    ["megadrive"]=".gen .md .smd"
    ["msx"]=".rom .mx1 .mx2"
    ["n64"]=".n64 .z64 .v64"
    ["neogeo"]=".zip"
    ["nes"]=".nes"
    ["ngp"]=".ngp"
    ["ngpc"]=".ngc"
    ["pcengine"]=".pce .cue"
    ["psp"]=".iso .cso"
    ["psx"]=".bin .cue .iso"
    ["sega32x"]=".32x"
    ["segacd"]=".iso .bin .cue"
    ["sg-1000"]=".sg"
    ["snes"]=".sfc .smc"
    ["vectrex"]=".vec"
    ["videopac"]=".bin"
    ["virtualboy"]=".vb"
    ["wonderswan"]=".ws"
    ["wonderswancolor"]=".wsc"
)

# Function to move files to their respective folders
move_files() {
    local folder=$1
    local extension=$2

    # Create folder if it doesn't exist
    mkdir -p "$folder"

    # Move files with the specified extension to the folder
    for file in *"$extension"; do
        # Prompt user to confirm source directory before moving
        read -p "Confirm source directory for $file [y/n]: " confirm
        if [[ $confirm == [yY] ]]; then
            mv "$file" "$folder"
        else
            echo "Skipping $file"
        fi
    done
}

# Iterate over each entry in the extensions array
for folder in "${!extensions[@]}"; do
    extensions_list="${extensions[$folder]}"
    read -ra extensions_arr <<< "$extensions_list"

    # Iterate over each extension in the current folder
    for ext in "${extensions_arr[@]}"; do
        # Move files with the extension to the respective folder
        move_files "$folder" "$ext"
    done
done
