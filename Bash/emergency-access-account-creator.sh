#!/bin/bash
#
#loveland
#
#Bash script that automates the creation and configuration of an Emergency Access Account,
#including modifying critical system files such as /etc/sudoers, /etc/pam.d/su, /etc/ssh/sshd_config,
#and /etc/pam.d/polkit-1. The script ensures backups are made for each file before modifications,
#appends the date, time, and a count of backups made on the same day to the backup filenames. It also
#checks for existing backups and prompts the user if they would prefer to restore the original
#configurations before proceeding.
#
#
##The script is based on nasa doc: https://cset.nasa.gov/ascs/handbook/linux-local-emergency-access-account-supplement-nasa-hdbk-2653-6-version-1-1/

#Chapter 6. Emergency Access Accounts
#All supplemental material can be found in Linux Emergency Access Account Supplement git repository.

#and will:

#Create a secure emergency access account (emgaccess).
#Configure system-wide security settings including:
#Sudo restrictions for other users.
#PAM configurations to secure access.
#SSH configuration to deny remote access to emgaccess.
#Generate a strong random passphrase for the account.
#Pseudocode
#Account Creation

#Create emgaccess user without a home directory and a random passphrase.
#Ensure the user does not belong to admin groups.
#Sudo Configuration

#Modify /etc/sudoers to restrict sudo access for emgaccess.
#PAM Configuration

#Adjust /etc/pam.d/common-auth to add security constraints specific to emgaccess.
#SSH Configuration

#Deny emgaccess SSH access by modifying /etc/ssh/sshd_config.
#Passphrase Generation
#
#
# Helper function to create backup of a file
#!/bin/bash

# Function to handle file backup

backup_file() {
    local file_path=$1
    local backup_dir=$(dirname $file_path)
    local backup_name=$(basename $file_path)
    local timestamp=$(date +%Y%m%d-%H%M%S)
    local count=$(ls ${backup_dir}/${backup_name}.* 2>/dev/null | wc -l | tr -d ' ')
    local backup_path="${file_path}.${timestamp}-${count}"

    if [ -f "$backup_path" ]; then
        echo "Backup already exists: $backup_path"
    else
        cp $file_path $backup_path
        echo "Backup created: $backup_path"
    fi
    return 0
}

# Function to restore a file from the most recent backup
restore_backup() {
    local file_path=$1
    local backup_dir=$(dirname $file_path)
    local backup_name=$(basename $file_path)
    local backups=($(ls ${backup_dir}/${backup_name}.* 2>/dev/null))
    if [ ${#backups[@]} -eq 0 ]; then
        echo "No backups found for $file_path."
        return 1
    else
        echo "Available backups for $file_path:"
        for i in "${!backups[@]}"; do
            echo "$((i+1))) ${backups[$i]}"
        done
        read -p "Select the backup to restore (number): " choice
        if [[ $choice =~ ^[0-9]+$ ]] && [ $choice -ge 1 ] && [ $choice -le ${#backups[@]} ]; then
            cp ${backups[$((choice-1))]} $file_path
            echo "Restoration complete: $file_path restored from ${backups[$((choice-1))]}"
        else
            echo "Invalid selection."
            return 1
        fi
    fi
}

# Main function to configure Emergency Access Account
configure_emergency_access() {
    local files=("/etc/sudoers" "/etc/pam.d/su" "/etc/ssh/sshd_config" "/etc/pam.d/polkit-1")

    for file in "${files[@]}"; do
        if [ -f "${file}.*" ]; then
            echo "Backup detected for $file"
            read -p "Do you want to restore the original file before proceeding? (y/N): " response
            if [[ "$response" =~ ^[Yy]$ ]]; then
                restore_backup $file
            fi
        fi
        backup_file $file

        # Add configurations here as needed
    done

    echo "Emergency Access Account configurations applied successfully."
}

# Function to setup emergency access account
setup_emergency_access() {
    configure_emergency_access
    echo "Setup complete."
}

# Start the script
echo "Emergency Access Account Management Script"
echo "1. Setup Emergency Access Account"
echo "2. Restore Original Configuration"
read -p "Enter your choice (1 or 2): " action

case $action in
    1) setup_emergency_access ;;
    2)
        for file in "/etc/sudoers" "/etc/pam.d/su" "/etc/ssh/sshd_config" "/etc/pam.d/polkit-1"; do
            restore_backup $file
        done
        ;;
    *) echo "Invalid option selected. Exiting." ;;
esac
