#!/bin/bash
#

read -r site_code <<< "$(cat /etc/miso/customer-miso-sitecode)"

mounted_drives=()
while IFS='' read -r line; do mounted_drives+=("$line"); done < <(lsblk -ln -o NAME,MOUNTPOINT | grep -E '^sd' | awk '{print $2}' | grep '^[[:blank:]]*[^[:blank:]#]')

if [ ${#mounted_drives[@]} -eq 0 ]; then
    echo "No external drives are currently mounted."
    exit 1
fi

if [ ${#mounted_drives[@]} -eq 1 ]; then
    read -r -a drive <<<"${mounted_drives[0]}"
else
    echo "Please select a mounted external drive:"
    select drive in "${mounted_drives[@]}"; do
        if [ -n "$drive" ]; then
            break
        else
            # Prompt user to try again
            echo "Invalid selection. Please try again."
        fi
    done
fi

backup_location=$drive/$site_code

echo
echo "*------------------------*"
echo "|Miso Migration Assistant|"
echo "*------------------------*"
echo
echo "This utility assists with the migration and restoration"
echo "of our compute nodes from one system to another."
echo
echo "Your backup location is $backup_location"
echo

read -p "Please choose: backup or restore? (type migrate or restore): " migres

    if [ "$migres" == "migrate" ]; then

		mkdir $backup_location
    	read -p "Is the backup going to be used on a brand new compute or are you going to be re-using the same compute? (please type new or same): " sameornew

		if [ "$sameornew" == "same" ]; then
			sudo rsync -ah --progress /etc/miso $backup_location/miso
			sudo rsync -ah --progress  /home/flippy/system-config /home/flippy/workspace $backup_location/flippy
			sudo rsync -ah --progress /var/lib/docker /var/lib/miso /var/lib/misorobotics $backup_location/lib
		elif [ "$sameornew" == "new" ]; then
			echo
			echo "Since you will be migrating data to a different machine, it is important to know that you will"
			echo "have to go through the steps needed to authenticate a new system which will download a new"
			echo "gcp-cred.json file when done"
			echo
			sudo rsync -ah --progress  /home/flippy/system-config /home/flippy/workspace $backup_location/flippy
			sudo rsync -ah --progress /var/lib/docker /var/lib/miso /var/lib/misorobotics $backup_location/lib
		else
	    	echo "No option selected. Exiting."
		fi

     elif [ "$migres" == "restore" ]; then
     	sudo rsync -ah --progress $backup_location/miso /data/etc/
	 	sudo rsync -ah --progress $backup_location/flippy/* /data/flippy/
	 	sudo rsync -ah --progress $backup_location/lib/* /data/var/lib/
     else
        echo "No option selected. Exiting."

     fi

echo
echo "Done!"
echo