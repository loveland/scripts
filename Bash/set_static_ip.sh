#!/bin/bash

STATIC_IP="192.168.1.2/24"
ROUTER_IP="192.168.1.1"
CONFIG_DONE_FLAG="/etc/network/config_done"

# Check if NetworkManager is installed
if ! command -v nmcli &> /dev/null; then
  echo "NetworkManager (nmcli) not found. Please install it or switch to ifupdown method."
  exit 1
fi

# Configure static IP for eth0 using NetworkManager (will run every time)
if ! nmcli connection show eth0 &> /dev/null; then
  echo "Creating new eth0 connection with static IP..."
  sudo nmcli con add type ethernet ifname eth0 con-name eth0 ipv4.addresses ${STATIC_IP} ipv4.gateway ${ROUTER_IP} ipv4.method manual
else
  echo "Modifying existing eth0 connection to use static IP..."
  sudo nmcli con mod eth0 ipv4.addresses ${STATIC_IP} ipv4.gateway ${ROUTER_IP} ipv4.method manual
fi

# Bring eth0 up
sudo nmcli con up eth0

# Check if wlan0 has been configured before
if [ ! -f "$CONFIG_DONE_FLAG" ]; then
  # Function to activate wlan0 only if eth0 is down
  activate_wlan() {
    # Check if eth0 is down and wlan0 is disconnected
    if nmcli device status | grep -q "eth0.*disconnected"; then
      echo "eth0 is down. Activating wlan0 with static IP..."
      sudo nmcli con up wlan0 || sudo nmcli con add type wifi ifname wlan0 con-name wlan0 ssid "YOUR_SSID" ipv4.addresses ${STATIC_IP} ipv4.gateway ${ROUTER_IP} ipv4.method manual
    else
      echo "eth0 is up. No need to activate wlan0."
    fi
  }

  # Run wlan0 logic if eth0 is down
  echo "First-time setup: Configuring wlan0..."
  activate_wlan

  # Create a flag file to indicate wlan0 setup is done
  sudo touch "$CONFIG_DONE_FLAG"
else
  echo "Skipping wlan0 configuration (already done)."
fi

# Check once to ensure wlan0 is activated only if eth0 is down
if nmcli device status | grep -q "eth0.*disconnected"; then
  echo "eth0 is down, activating wlan0..."
  sudo nmcli con up wlan0
fi
