#!/bin/bash

# Define the list of ports to check, excluding SSH (22) and VNC (5901)
ports=(1 11 111 139 143 540 6667 12345 27665 32771 32773 32774)

# Array to store found service names
services=()

# Function to check the service running on a specified port
check_service() {
    local port=$1

    # Check for the service using ss and systemctl
    if service_name=$(sudo ss -tulnp | grep ":$port" | awk '{print $NF}' | sed 's/.*,//' | sed 's/"//g'); then
        if systemctl list-units --type=service --all | grep -q "$service_name.service"; then
            services+=("$service_name")
        fi
    fi
}

# Loop through each port and check for services
for port in "${ports[@]}"; do
    check_service "$port"
done

# Print the simplified list of service names
echo "Services running on specified ports that can be stopped and disabled with systemctl:"
for service in "${services[@]}"; do
    echo "$service"
done

# Avoid duplicates in case the same service runs on multiple ports
unique_services=($(printf "%s\n" "${services[@]}" | sort -u))

# Command summary to stop and disable the services
echo -e "\nTo stop and disable these services, you can run the following commands:"
for service in "${unique_services[@]}"; do
    echo "sudo systemctl stop $service && sudo systemctl disable $service"
done

