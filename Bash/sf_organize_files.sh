#!/bin/bash

# Function to organize files into folders by extension
organize_files() {
    local folder="$1"

    # Check if the folder exists
    if [ ! -d "$folder" ]; then
        echo "Error: Folder '$folder' not found."
        exit 1
    fi

    # Navigate to the specified folder
    cd "$folder" || exit 1

    # Loop through all files in the folder and its subfolders
    find . -type f | while IFS= read -r file; do
        # Get the file extension
        extension="${file##*.}"
        
        # Ignore files without an extension
        if [ "$file" != "${file%.*}" ]; then
            # Create directory if it doesn't exist
            mkdir -p "$extension"
            # Move the file to its corresponding directory
            mv "$file" "$extension/"
        fi
    done

    echo "Files organized successfully."
}

# Function to organize folders into categories
organize_folders() {
    local folder="$1"

    # Create directories for categories
    mkdir -p "$folder/Pictures" "$folder/Documents" "$folder/Videos" "$folder/Music" "$folder/Others"

    # Move folders to respective categories
    for dir in "$folder"/*/; do
        case "$(basename "$dir")" in
            "png" | "jpg" | "jpeg" | "gif" | "bmp" | "svg" )
                mv "$dir" "$folder/Pictures/" ;;
            "doc" | "docx" | "pdf" | "txt" | "xls" | "xlsx" | "ppt" | "pptx" )
                mv "$dir" "$folder/Documents/" ;;
            "mp4" | "avi" | "mkv" | "mov" | "wmv" | "flv" )
                mv "$dir" "$folder/Videos/" ;;
            "mp3" | "wav" | "ogg" | "flac" | "aac" | "wma" )
                mv "$dir" "$folder/Music/" ;;
            *)
                mv "$dir" "$folder/Others/" ;;
        esac
    done

    echo "Folders organized into categories successfully."
}

# Function to delete empty folders
delete_empty_folders() {
    local folder="$1"
    
    # Find and delete empty folders recursively
    find "$folder" -type d -empty -delete

    echo "Empty folders deleted."
}

# Function to delete .DS_Store files and folders
delete_ds_store() {
    local folder="$1"

    # Delete .DS_Store files recursively
    find "$folder" -name ".DS_Store" -type f -delete

    # Delete .DS_Store folders recursively
    find "$folder" -name ".DS_Store" -type d -exec rm -rf {} +
    
    echo ".DS_Store files and folders deleted."
}

# Check if a folder path is provided as an argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <folder_path>"
    exit 1
fi

# Call the function with the provided folder path
organize_files "$1"

# Ask the user if they want to organize the folders into categories
read -r -p "Do you want to organize the folders into categories? (y/n): " choice
case "$choice" in
    [yY]|[yY][eE][sS])
        organize_folders "$1"
        ;;
    *)
        echo "Folders not organized into categories."
        ;;
esac

# Delete .DS_Store files and folders
delete_ds_store "$1"

# Delete empty folders
delete_empty_folders "$1"

