brew install binutils
brew install diffutils
brew install ed 
brew install findutils
brew install gawk
brew install gnu-indent 
brew install gnu-sed 
brew install gnu-tar 
brew install gnu-which 
brew install gnutls
brew install grep 
brew install gzip
brew install screen
brew install watch
brew install wdiff
brew install wget
brew install gnupg
brew install gnupg2
brew install bash
brew install --cocoa --srgb emacs
brew linkapps emacs
brew install gdb
brew install guile
brew install gpatch
brew install m4
brew install make
brew install nano
brew install file-formula
brew install git
brew install less
brew install openssh
brew install rsync
brew install svn
brew install unzip
brew install vim --override-system-vi
brew install macvim --with-override-system-vim --custom-system-icons
brew link --overwrite macvim
brew linkapps macvim
brew install zsh
brew tap homebrew/versions
brew install perl518
echo "brew install ed" >> ~/.bashrc
echo "brew install findutils" >> ~/.bashrc
echo "brew install gnu-indent" >> ~/.bashrc
echo "brew install gnu-sed" >> ~/.bashrc
echo "brew install gnu-tar" >> ~/.bashrc
echo "brew install gnu-which" >> ~/.bashrc
echo "brew install grep" >> ~/.bashrc
echo 'PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"' >> ~/.bashrc
echo 'PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"' >> ~/.bashrc
echo 'PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"' >> ~/.bashrc
echo 'MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"' >> ~/.bashrc
echo 'MANPATH="/usr/local/opt/gnu-sed/libexec/gnuman:$MANPATH"' >> ~/.bashrc
echo 'MANPATH="/usr/local/opt/gnu-tar/libexec/gnuman:$MANPATH"' >> ~/.bashrc
brew install wget git sqlite svn
