#!/bin/bash

# Packages to keep (GNOME and KDE Plasma)
keep_sessions=("gnome-session" "startplasma-x11")

# All session managers (based on your current list)
all_sessions=(
    "gnome-session"
    "i3"
    "lxsession"
    "mate-session"
    "openbox"
    "startlxde"
    "startplasma-x11"
    "startxfce4"
    "xfce4-session"
    "gnome-flashback-metacity"
)

# Loop through all sessions and remove those not in the keep_sessions list
for session in "${all_sessions[@]}"; do
    if [[ ! " ${keep_sessions[@]} " =~ " ${session} " ]]; then
        echo "Removing $session..."
        sudo apt remove --purge -y $session
    fi
done

# Clean up any unnecessary packages after removing the session managers
sudo apt autoremove -y
sudo apt clean

echo "Unwanted session managers removed, GNOME Classic and Plasma are kept."

