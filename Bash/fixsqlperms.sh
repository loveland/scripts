chown mysql.mysql -R /var/lib/mysql
cd /var/lib/mysql
find . -type d -print0 | xargs -0 chmod 700
find . -type f -print0 | xargs -0 chmod 660
chmod 777 *.sock
/etc/init.d/mysqld restart
