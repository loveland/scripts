#!/bin/bash

# Exit immediately if any command exits with a non-zero status
set -e

# Usage: ./safe_copy_with_logging.sh <source_directory> <destination_directory>
SOURCE=$1
DEST=$2

# Check if both arguments are provided
if [ -z "$SOURCE" ] || [ -z "$DEST" ]; then
    echo "Usage: $0 <source_directory> <destination_directory>"
    exit 1
fi

# Resolve absolute paths for source and destination
SOURCE=$(realpath "$SOURCE")
DEST=$(realpath "$DEST")

# Check if source and destination are the same
if [ "$SOURCE" == "$DEST" ]; then
    echo "Error: Source and destination directories are the same. Exiting."
    exit 1
fi

# Check if destination is inside the source directory
if [[ "$DEST" == "$SOURCE"* ]]; then
    echo "Error: Destination cannot be a subdirectory of the source. Exiting."
    exit 1
fi

# Create the destination directory if it doesn't exist
mkdir -p "$DEST"

# Log the operation
LOG_FILE="./copy_log_$(date +%Y%m%d_%H%M%S).log"
echo "Starting copy operation..." | tee -a "$LOG_FILE"
echo "Source: $SOURCE" | tee -a "$LOG_FILE"
echo "Destination: $DEST" | tee -a "$LOG_FILE"

# Perform the copy operation and log each file/directory
rsync -av --exclude "$DEST" "$SOURCE/" "$DEST/" \
    --log-file="$LOG_FILE" --info=progress2

echo "Copy completed successfully! Log saved to $LOG_FILE"

