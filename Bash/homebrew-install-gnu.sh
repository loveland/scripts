# see https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

# core
brew install coreutils

# key commands
brew install binutils
brew install diffutils

brew install ed
export PATH="/usr/local/opt/ed/libexec/gnubin:$PATH"

brew install findutils
export PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"

brew install gawk
brew install gnu-indent
export PATH="/usr/local/opt/gnu-indent/libexec/gnubin:$PATH"

brew install gnu-sed
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"

brew install gnu-tar
export PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"

brew install gnu-which
export PATH="/usr/local/opt/gnu-which/libexec/gnubin:$PATH"

brew install gnutls
brew install grep
export PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"

brew install gzip
brew install screen
brew install watch
brew install wdiff
export PATH="/usr/local/opt/wdiff/libexec/gnubin:$PATH"

brew install wget

# OS X ships a GNU version, but too old
brew install bash
brew install emacs
brew install gdb  # gdb requires further actions to make it work. See `brew info gdb`.
brew install gpatch
brew install m4
brew install make
brew install nano

# Other commands (non-GNU)
brew install file-formula
brew install git
brew install less
brew install openssh
brew install perl518   # must run "brew tap homebrew/versions" first!
brew install python
brew install rsync
brew install svn
brew install unzip
brew install vim --override-system-vi
brew install macvim --override-system-vim --custom-system-icons
brew install zsh