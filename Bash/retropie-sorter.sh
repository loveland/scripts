#!/usr/bin/env bash

# Prompt for the destination directory or use the current directory
read -p "Enter destination directory or press Enter to use current directory: " input_destination
destination_folder=${input_destination:-$(pwd)}

# Check if the source folder is provided as an argument; if not, prompt for it
if [ -z "$1" ]; then
    read -p "Enter source folder or press Enter to use current directory: " source_folder
    source_folder=${source_folder:-$(pwd)}
else
    source_folder="$1"
fi

if ! mkdir -p "$destination_folder"; then
    echo "Error: Failed to create destination directory at '$destination_folder'."
    exit 1
fi

declare -A extensions=(
    [".bin"]="atari2600 atari5200 videopac"
    [".a78"]="atari7800"
    [".atr"]="atari800"
    [".xex"]="atari800"
    [".rom"]="atari800 msx"
    [".lnx"]="atarilynx"
    [".d64"]="c64"
    [".t64"]="c64"
    [".crt"]="c64"
    [".cdi"]="dreamcast"
    [".gdi"]="dreamcast"
    [".gg"]="gamegear"
    [".gb"]="gb"
    [".gba"]="gba"
    [".gbc"]="gbc"
    [".sms"]="mastersystem"
    [".gen"]="megadrive"
    [".md"]="megadrive"
    [".smd"]="megadrive"
    [".n64"]="n64"
    [".z64"]="n64"
    [".v64"]="n64"
    [".zip"]="neogeo"
    [".nes"]="nes"
    [".ngp"]="ngp"
    [".ngc"]="ngpc"
    [".pce"]="pcengine"
    [".cue"]="pcengine segacd psx"
    [".iso"]="pcengine psp psx segacd"
    [".cso"]="psp"
    [".32x"]="sega32x"
    [".sg"]="sg-1000"
    [".sfc"]="snes"
    [".smc"]="snes"
    [".vec"]="vectrex"
    [".vb"]="virtualboy"
    [".ws"]="wonderswan"
    [".wsc"]="wonderswancolor"
)

unknown_roms_folder="$destination_folder/Unknown ROMs"

# Function to move files to the best-guessed system directory
function move_files() {
    find "$source_folder" -type f \( $(printf -- "-name *%s -o " "${!extensions[@]}") -name "*${!extensions[@]: -1}" \) | while read -r file; do
        local base_name=$(basename "$file")
        local ext=".${base_name##*.}"
        local possible_systems="${extensions[$ext]}"
        local guessed_system=$(guess_system "$base_name" "$possible_systems")
        local system_path="$destination_folder/$guessed_system"
        mkdir -p "$system_path"
        echo "Moving $file to $system_path"
        mv "$file" "$system_path/"
    done
}

# Function to guess the system based on filename heuristics
function guess_system() {
    local filename=$1
    local systems=$2
    local system_guess="Unknown ROMs"  # Default if no match is found
    for system in $systems; do
        if [[ "$filename" == *"$system"* ]] || [[ "$filename" == *"${system^^}"* ]]; then
            system_guess=$system
            break
        fi
    done
    echo $system_guess
}

# Function to handle unknown files
function handle_unknown_files() {
    echo "Sorting unknown files..."
    find "$source_folder" -type f | while read -r file; do
        local base_name=$(basename "$file")
        if [[ ! "$base_name" =~ \.(${known_extensions// /|})$ ]]; then
            local console_guess=$(echo "$base_name" | grep -oE 'atari|nes|snes|gameboy|megadrive' | head -n 1)
            console_guess=${console_guess:-"Misc"}
            local unknown_console_path="$unknown_roms_folder/$console_guess"
            mkdir -p "$unknown_console_path"
            echo "Moving unknown file $base_name to $unknown_console_path"
            mv "$file" "$unknown_console_path/"
        fi
    done
}

known_extensions=$(printf "%s " "${!extensions[@]}" | sed 's/ /\|/g')
move_files
handle_unknown_files
echo "ROM organization complete. Files moved to $destination_folder."
