#!/bin/bash

hdiutil attach ~/Downloads/hpdrivers.dmg

pkgutil --expand /Volumes/HP_PrinterSupportManual/HewlettPackardPrinterDrivers.pkg ~/Downloads/hp-expand

hdiutil eject /Volumes/HP_PrinterSupportManual

sed -i '' 's/15.0/16.0/' ~/Downloads/hp-expand/Distribution

pkgutil --flatten ~/Downloads/hp-expand ~/Downloads/HP_Drivers_15.0.1.pkg

rm -R ~/Downloads/hp-expand
