#!/bin/sh
#
# Bash script requested using curl to populate digital ocean droplets for DYL staging (legacy). 
# Ansible role also created to populate entire suite of latest droplets required for DYL staging (Dyl-Staging-Init.yml) currently being used as primary build technique. 
#
# Author: Loveland
#

echo "Shell Script to populate Digital Ocean Droplets for DYL Staging"
read -p "Input Digital Ocean Token: " TOKEN
read -p "Input User Initials: " USERINITIALS
read -p "Input SSH Key Fingerprint (enter list to display all possible options stored in account or enter 0 for null): " SSH_KEY_FP

DNAME1=dyl1-$USERINITIALS.getdyl.com  #40 
DNAME2=lb1-$USERINITIALS.getdyl.com   #5
DNAME3=ctl1-$USERINITIALS.getdyl.com  #5
DNAME4=ctl2-$USERINITIALS.getdyl.com  #5
DNAME5=pbx1-$USERINITIALS.getdyl.com  #10
DNAME6=pbx2-$USERINITIALS.getdyl.com  #10
DNAME7=inb1-$USERINITIALS.getdyl.com  #5
DNAME8=lcr1-$USERINITIALS.getdyl.com  #5
DNAME9=sbc1-$USERINITIALS.getdyl.com  #5
DNAME10=vpm1-$USERINITIALS.getdyl.com #5
DNAME11=nag1-$USERINITIALS.getdyl.com #5

if [ "$SSH_KEY_FP" == "list" ]; then
    unset SSH_KEY_FP
    echo ""
    curl --request GET "https://api.digitalocean.com/v2/account/keys" \
         --header "Authorization: Bearer $TOKEN"
    echo ""
    read -p "Input SSH Key Fingerprint (enter list to display all possible options stored in account or enter 0 for null): " SSH_KEY_FP
fi

if (( $SSH_KEY_FP == 0 )); then
    #Single 4GB Droplet Creation
    echo ""
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'"$DNAME1"'","region":"sfo1","size":"4GB","image":"centos-6-5-x64","ssh_keys":null,"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"volumes": null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
    #Multiple 1GB Droplets (2)
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"names":["'"$DNAME5"'","'"$DNAME6"'"],"region":"sfo1","size":"1GB","image":"centos-6-5-x64","ssh_keys":null,"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
    #Multiple 512mb Droplets (8)
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"names":["'"$DNAME2"'","'"$DNAME3"'","'"$DNAME4"'","'"$DNAME7"'","'"$DNAME8"'", "'"$DNAME9"'", "'"$DNAME10"'", "'"$DNAME11"'"],"region":"sfo1","size":"512MB","image":"centos-6-5-x64","ssh_keys":null,"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
else
    echo ""
    #Single 4GB Droplet Creation
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'"$DNAME1"'","region":"sfo1","size":"4GB","image":"centos-6-5-x64","ssh_keys":[ "'"$SSH_KEY_FP"'" ],"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"volumes": null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
    #Multiple 1GB Droplets (2)
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"names":["'"$DNAME5"'","'"$DNAME6"'"],"region":"sfo1","size":"1GB","image":"centos-6-5-x64","ssh_keys":[ "'"$SSH_KEY_FP"'" ],"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
    #Multiple 512mb Droplets (8)
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"names":["'"$DNAME2"'","'"$DNAME3"'","'"$DNAME4"'","'"$DNAME7"'","'"$DNAME8"'", "'"$DNAME9"'", "'"$DNAME10"'", "'"$DNAME11"'"],"region":"sfo1","size":"1GB","image":"centos-6-5-x64","ssh_keys":[ "'"$SSH_KEY_FP"'" ],"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"tags":["staging"]}' "https://api.digitalocean.com/v2/droplets"
fi

echo ""
echo "Successfully created the following droplets:"
echo ""

# could use wrapper "curl-all-pages" if installed instead of calling 3 curls or implement a loop
curl -X GET "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=2" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=3" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'

