#!/bin/bash
#loveland
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@" 
fi
hexchars="0123456789ABCDEF"
end=$( for i in {1..10} ; do echo -n ${hexchars:$(( $RANDOM % 16 )):1} ; done | sed -e 's/\(..\)/:\1/g' )
MAC=00$end
 
service network-manager stop
ifconfig en0 down
ifconfig en0 hw ether $MAC
ifconfig en0 up
service network-manager start
 
echo $MAC
