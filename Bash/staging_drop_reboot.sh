#!/bin/sh
#
# Bash script using curl to reboot, shutdown, poweroff or poweron  a digital ocean droplet for DYL using apiv2. 
#
# Author: Loveland
#
# droplet_ids = array tag to list droplet_ids associated with droplets need to put into get request still 
#

echo "Reboot, Shutdown, Poweroff, or Poweron Digital Ocean Droplet for DYL Staging"
read -p "Input Digital Ocean Token: " TOKEN
read -p "Input User Initials: " USERINITIALS


# could use wrapper "curl-all-pages" if installed instead of calling 3 curls or implement a loop
curl -X GET "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=2" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=3" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=4" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=5" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=6" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=7" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'



echo ""
read -p "Input Droplet ID: " ID
read -p "Choose: 1=Reboot 2=Shutdown 3=Power Off 4=Power On" OPT

if (( $OPT  == 1 )); then
#Single Droplet Reboot
curl -X POST -H "Content-Type: application/json" \
             -H "Authorization: Bearer $TOKEN" \
             -d '{"type":"reboot"}' \
             "https://api.digitalocean.com/v2/droplets/$ID/actions"

elif (( $OPT  == 2 )); then
#Single Droplet Shutdown
curl -X POST -H "Content-Type: application/json" \
             -H "Authorization: Bearer $TOKEN" \
             -d '{"type":"shutdown"}' \
             "https://api.digitalocean.com/v2/droplets/$ID/actions"

elif (( $OPT  == 3 )); then
#Single Droplet Power Off
curl -X POST -H "Content-Type: application/json" \
             -H "Authorization: Bearer $TOKEN" \
             -d '{"type":"power_off"}' \
             "https://api.digitalocean.com/v2/droplets/$ID/actions"

elif (( $OPT  == 4 )); then
#Single Droplet Power On
curl -X POST -H "Content-Type: application/json" \
             -H "Authorization: Bearer $TOKEN" \
             -d '{"type":"power_on"}' \
             "https://api.digitalocean.com/v2/droplets/$ID/actions"
else
   
echo "no option selected. quit."

fi


