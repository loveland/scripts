#!/bin/bash

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root (use sudo)."
  exit 1
fi

echo "Welcome to the Wi-Fi Extender Setup Script!"

# Update the system
echo "Updating system packages..."
apt update && apt upgrade -y

# Install required packages
echo "Installing required packages..."
apt install -y hostapd dnsmasq bridge-utils iw dialog

# Gather user input
read -p "Enter the name (SSID) for your Wi-Fi extender: " EXTENDER_SSID
read -sp "Enter a password for your Wi-Fi extender (min 8 characters): " EXTENDER_PASSWORD
echo
if [[ ${#EXTENDER_PASSWORD} -lt 8 ]]; then
  echo "Password must be at least 8 characters. Exiting..."
  exit 1
fi

# Configure dhcpcd
echo "Configuring network settings..."
echo "denyinterfaces wlan0" >> /etc/dhcpcd.conf
systemctl restart dhcpcd

# Set up network bridge
echo "Setting up network bridge..."
cat <<EOF > /etc/network/interfaces.d/bridge
auto br0
iface br0 inet dhcp
    bridge_ports wlan0
EOF

# Configure hostapd
echo "Configuring the access point..."
cat <<EOF > /etc/hostapd/hostapd.conf
interface=wlan1
driver=nl80211
ssid=$EXTENDER_SSID
hw_mode=g
channel=6
wmm_enabled=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=$EXTENDER_PASSWORD
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
EOF

# Set the hostapd daemon configuration file
sed -i 's|#DAEMON_CONF=""|DAEMON_CONF="/etc/hostapd/hostapd.conf"|' /etc/default/hostapd

# Configure dnsmasq
echo "Configuring the DHCP server..."
mv /etc/dnsmasq.conf /etc/dnsmasq.conf.bak
cat <<EOF > /etc/dnsmasq.conf
interface=wlan1
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
EOF

# Enable and start services
echo "Enabling and starting services..."
systemctl unmask hostapd
systemctl enable hostapd
systemctl enable dnsmasq
systemctl start hostapd
systemctl start dnsmasq

# Signal strength dialog setup
echo "Setting up the signal strength monitoring tool..."
cat <<'EOF' > /usr/local/bin/signal_strength
#!/bin/bash
while true; do
    SIGNAL=$(iw dev wlan0 link | grep 'signal' | awk '{print $2}')
    clear
    echo "Wi-Fi Extender Signal Strength: $SIGNAL dBm"
    echo "Press Ctrl+C to exit."
    sleep 2
done
EOF
chmod +x /usr/local/bin/signal_strength

# Completion message
echo "Setup complete! Your Wi-Fi extender is ready."
echo "To monitor signal strength, run the command: signal_strength"

# Reboot to apply all changes
read -p "Setup complete! Would you like to reboot now? (y/n): " REBOOT_NOW
if [[ $REBOOT_NOW == "y" || $REBOOT_NOW == "Y" ]]; then
  echo "Rebooting now..."
  reboot
else
  echo "Please reboot your system later to apply all changes."
fi

