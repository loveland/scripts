#!/usr/bin/env bash

# Navigate to the directory you want to organize, or use "." to represent the current directory
cd ./ || exit

# Loop over all files in the current directory
for file in *.*; do
    # Skip directories
    if [ -d "$file" ]; then
        continue
    fi

    # Extract the extension of the file
    ext="${file##*.}"

    # Convert to lowercase if necessary (optional)
    # ext="${ext,,}"

    # Create a directory for the extension if it does not exist
    mkdir -p "$ext"

    # Move the file into its corresponding extension directory
    mv "$file" "$ext/"
done

echo "Files have been organized by extension."

