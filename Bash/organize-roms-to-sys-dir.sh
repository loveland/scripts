#!/usr/bin/env bash

# Define the destination base directory
destination_base="./roms"

# Ensure the base directory exists
mkdir -p "$destination_base"

# Define the extension to system mapping
declare -A extensions=(
    [".bin"]="atari2600 atari5200 videopac"
    [".a78"]="atari7800"
    [".atr"]="atari800"
    [".xex"]="atari800"
    [".rom"]="atari800 msx"
    [".lnx"]="atarilynx"
    [".d64"]="c64"
    [".t64"]="c64"
    [".crt"]="c64"
    [".cdi"]="dreamcast"
    [".gdi"]="dreamcast"
    [".gg"]="gamegear"
    [".gb"]="gb"
    [".gba"]="gba"
    [".gbc"]="gbc"
    [".sms"]="mastersystem"
    [".gen"]="megadrive"
    [".md"]="megadrive"
    [".smd"]="megadrive"
    [".n64"]="n64"
    [".z64"]="n64"
    [".v64"]="n64"
    [".zip"]="neogeo"
    [".nes"]="nes"
    [".ngp"]="ngp"
    [".ngc"]="ngpc"
    [".pce"]="pcengine"
    [".cue"]="pcengine segacd psx"
    [".iso"]="pcengine psp psx segacd"
    [".cso"]="psp"
    [".32x"]="sega32x"
    [".sg"]="sg-1000"
    [".sfc"]="snes"
    [".smc"]="snes"
    [".vec"]="vectrex"
    [".vb"]="virtualboy"
    [".ws"]="wonderswan"
    [".wsc"]="wonderswancolor"
)

# Function to guess the system based on file extension
guess_system() {
    local ext=$1
    IFS=' ' read -r -a systems <<< "${extensions[$ext]}"
    if [ ${#systems[@]} -eq 1 ]; then
        echo "${systems[0]}"
    else
        echo "${systems[0]}"  # Default to the first system if multiple options exist
    fi
}

# Main loop to move files
find . -type f | while read -r file; do
    ext=".${file##*.}"
    if [[ -n "${extensions[$ext]}" ]]; then
        system=$(guess_system "$ext")
        target_dir="$destination_base/$system"
        mkdir -p "$target_dir"
        mv "$file" "$target_dir/"
        echo "Moved $file to $target_dir"
    fi
done
