#!/bin/sh
#
#Bash script using curl to populate digital ocean droplet for DYL Dev-Web Access 512MB Servers
#Author: Loveland
#
#
#For ansible use, run script from localhost via playbook and include vars below from export. 
#Otherwise, just run script to input necessary variables from prompt.
#
###
USERINITIALS=0

if [ "$USERINITIALS" = "0" ]
then
    echo "Shell Script to populate 512MB Digital Ocean Droplet for DYL Developer Web Access"
    read -p "Input user name or initials: (ex. dib ..will be dib-web.getdyl.com) " USERINITIALS
    read -p "Input Digital Ocean Token: " TOKEN
    read -p "Input SSH Key Fingerprint (enter list to display all possible options stored in account or enter 0 for null): " SSH_KEY_FP
else
    DNAME1=$USERINITIALS-web.getdyl.com
fi

if [ "$SSH_KEY_FP" == "list" ] 
then
    unset SSH_KEY_FP
    echo ""
    curl --request GET "https://api.digitalocean.com/v2/account/keys" \
         --header "Authorization: Bearer $TOKEN"
    echo ""
    read -p "Input SSH Key Fingerprint (enter list to display all possible options stored in account or enter 0 for null): " SSH_KEY_FP
elif [ "$SSH_KEY_FP" = "0" ]; then
    #Single 512MB Droplet Creation
    echo "Creating droplet without ssh-key"
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'"$DNAME1"'","region":"sfo1","size":"512MB","image":"centos-7-x64","ssh_keys":null,"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"volumes": null,"tags":["dev-web"]}' "https://api.digitalocean.com/v2/droplets"
else
    echo "Creating droplet with ssh-key fingerprint $SSH_KEY_FP"
    #Single 512MB Droplet Creation
    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'"$DNAME1"'","region":"sfo1","size":"512MB","image":"centos-7-x64","ssh_keys":[ "'"$SSH_KEY_FP"'" ],"backups":false,"ipv6":false,"user_data":null,"private_networking":null,"volumes": null,"tags":["dev-web"]}' "https://api.digitalocean.com/v2/droplets"
fi

echo ""
echo "Successfully created the following droplet:"
echo ""

# could use wrapper "curl-all-pages" if installed instead of calling 3 curls or implement a loop
curl -X GET "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=2" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'
curl -X GET "https://api.digitalocean.com/v2/droplets?page=3" -H "Authorization: Bearer $TOKEN" | jq -r '.droplets | .[] | select(.name | contains("'"$USERINITIALS"'")) | .name , (.networks .v4 | .[] .ip_address)'

