#/bin/bash

echo
echo "isoit®! ~ Cross Platform .iso creation made easy. ~ ©SuperFreshCo." | lolcat
echo "------------------------------------------------------------------" | lolcat
echo
echo "Convert to iso:" | lolcat
read -p "	Path to file or folder: " ptf
isoloc=$ptf'.iso'

hdiutil makehybrid -iso -joliet -o $ptf $isoloc
echo "Complete!"
exit