import SwiftUI

struct ContentView: View {
    @State private var sourceFolder: String = ""
    @State private var logText: String = "Press Start to organize ROMs"

    var body: some View {
        VStack {
            TextField("Source Folder", text: $sourceFolder)
                .padding()

            Button("Start") {
                organizeROMs()
            }
            .padding()

            ScrollView {
                Text(logText)
                    .padding()
            }
        }
        .frame(width: 400, height: 300)
    }

    func organizeROMs() {
        let task = Process()
        task.launchPath = "/bin/bash"
        task.arguments = ["path/to/your/script.sh", sourceFolder]

        let pipe = Pipe()
        task.standardOutput = pipe
        task.standardError = pipe

        task.launch()

        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(decoding: data, as: UTF8.self)

        DispatchQueue.main.async {
            self.logText = output
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
