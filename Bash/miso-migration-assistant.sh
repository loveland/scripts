#!/bin/bash
#
# NOTE: This is meant for migrations from our single os systems to our dual os systems. If trying to migrate from one dual os to another dual os
# You can try to add the -L flag to the rsync commands to follow symlinks and copy the files contained in the symlinks but this also proves tricky as 
# there will be instances of too many symbolic links. The --ignore-existing flag was added to the backup in the case where a user may need to resume
# an interrupted backup. the --update flag was used on restore to assist in restorations where only files that have been changed will be copied.
#

# Visual help


BOLD="\033[1m"
RED="\033[31m"
GREEN="\033[32m"
MAGENTA="\033[35m"
CYAN="\033[36m"
ENDFM="\033[0m"

stopServices () {
    f3-stop
    systemctl stop docker.socket
}

startServices () {
    systemctl start docker.socket
    systemctl start docker
    f3-start
}

# Make sure the script is running as sudo or root
if [ "$(id -u)" != 0 ]; then
    echo -e "${BOLD}${RED}ERROR:${ENDFM}
    Please run this script with the correct privliages.
    It requires to be ran with sudo or as root."
    exit 1
fi

echo -e "${BOLD}${CYAN}
*------------------------*
|Miso Backup Assistant   |
*------------------------*

This script assists with backing up and restoring
data on our compute nodes for Flippy.
${END}
"

read -r site_code <<< "$(cat /etc/miso/customer-miso-sitecode)"

mounted_drives=()
while IFS='' read -r line; do mounted_drives+=("$line"); done < <(lsblk -ln -o NAME,MOUNTPOINT | grep -E '^sd' | awk '{print $2}' | grep '^[[:blank:]]*[^[:blank:]#]')

if [ ${#mounted_drives[@]} -eq 0 ]; then
    echo -e "${BOLD}${RED}ERROR:${ENDFM}
    No external drives are currently mounted.
    Please attach and mount a drive and run this script again"
    exit 1
fi

if [ ${#mounted_drives[@]} -eq 1 ]; then
    read -r -a drive <<<"${mounted_drives[0]}"
else
    echo -e "${BOLD}${CYAN}Please select a mounted external drive:${ENDFM}"
    select drive in "${mounted_drives[@]}"; do
        if [ -n "$drive" ]; then
            break
        else
            # Prompt user to try again
            echo -e "${MAGENTA}Invalid selection. Please try again.${ENDFM}"
        fi
    done
fi

backup_location=$drive/$site_code

# Print out the location of where the files will be backed up to or restored from
echo -e "Your backup location will be ${BOLD}${GREEN}$backup_location${ENDFM}\n"

read -rp "$(echo -e "${BOLD}${CYAN}Please type backup or restore:${ENDFM} ")" migration_type

    if [ "$migration_type" == "backup" ]; then

		mkdir "$backup_location"
    	read -rp "$(echo -e "${BOLD}${CYAN}Is the backup going to be restored on a brand new compute or are you going to be re-using the same compute? (please type new or same):${ENDFM} ")" sameornew

		if [ "$sameornew" == "same" ]; then
			stopServices
			rsync -ah --ignore-existing --stats --info=progress2 /home/flippy /etc/miso /etc/ssh "$backup_location"/
			rsync -ah --ignore-existing --stats --info=progress2 /var/lib/docker /var/lib/miso /var/lib/misorobotics "$backup_location"/lib/
            # startServices
		elif [ "$sameornew" == "new" ]; then
            stopServices
			rsync -ah --ignore-existing --stats --info=progress2 /home/flippy /etc/ssh "$backup_location"/
			rsync -ah --ignore-existing --stats --info=progress2 /var/lib/docker /var/lib/miso /var/lib/misorobotics "$backup_location"/lib
            # startServices
		else
	    	echo -e "${BOLD}${RED}No option selected. Exiting.${ENDFM}"
		fi

     elif [ "$migration_type" == "restore" ]; then
        echo "Stoping all docker services"
        stopServices
        echo -e "Restoring ${BOLD}/etc/miso${ENDFM} ..."
        rm /etc/miso/deviceid
        rsync -ah --update --stats --info=progress2 "$backup_location"/miso /data/etc/
        echo -e "Restoring ${BOLD}/etc/ssh${ENDFM} ..."
        rsync -ah -I --stats --info=progress2 "$backup_location"/ssh/ssh*host*key* /etc/ssh
        echo -e "Restoring ${BOLD}/home/flippy${ENDFM} ..."
	 	rsync -ah -I --stats --info=progress2 "$backup_location"/flippy/* /data/flippy/
        echo -e "Restoring directories in ${BOLD}/var/lib/${ENDFM} ..."
	 	rsync -ah -I --stats --info=progress2 --delete "$backup_location"/lib/* /data/var/lib/
        # startServices
     else
        echo -e "${BOLD}${RED}No option selected. Exiting.${ENDFM}"

     fi

echo "Done!"