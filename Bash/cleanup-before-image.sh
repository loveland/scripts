#!/bin/bash

# run this script before creating a clonezilla image of 
# a staging compute node.
# This ensure that no device-specific files are included in the image


for dir in /root /home/flippy ; do
	echo cleaning up $dir
	cd $dir
	/bin/rm -rf .bash_history .cache .miso-docker-secrets .ssh
	/bin/rm -rf .config/gcloud/credentials.db
	/bin/rm -rf .config/gcloud/access_tokens.db
done

echo removing deviceid and related things under /etc/miso

/bin/rm -rf /etc/miso/deviceid /etc/miso/secrets/gcp-cred.json
/bin/rm -rf /etc/miso/customer-gcp-project /etc/miso/secrets/datadog-api-key


echo cleaning up network stuffs..
if test -f /etc/network/interfaces ; then
  echo erasing network config
  cat >/etc/network/interfaces <<EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback
EOF
fi 

egrep -v 'router|pdu|robot' /etc/hosts >/tmp/hosts.tmp
cp /tmp/hosts.tmp /etc/hosts
/bin/rm /tmp/hosts.tmp
