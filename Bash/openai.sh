OPENAI_API_KEY="sk-proj-Bw3C3CV8LJFkTypeXeVTT3BlbkFJ6Vf3ZjlobcJWTEyVpSMm"

curl "https://api.openai.com/v1/assistants" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -H "OpenAI-Beta: assistants=v2" \
  -d '{
    "instructions": "You are a personal math tutor. When asked a question, write and run Python code to answer the question.",
    "name": "Math Tutor",
    "tools": [{"type": "code_interpreter"}],
    "model": "gpt-3.5-turbo"
  }'