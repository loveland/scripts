#
# -----------------------------------------------------------------------
# "s2np" = String To Numerical Permissions Converter Tool
#
# convert string permissions such as -rwxr-xr-x to numerical expression
# loveland
#
# eg. s2np file (or dir, *, etc)
#
# -------------------------------------------------------------------------
#

stat -c %a ./

#
# The More You Know
# 
# r=4
# w=2
# x=1
#
# in every group. Your example is 6(r+w=4+2)4(r=4)4(r=4).
# The full permissions mode number is a 4-digit octal number, though most of the# time, you only use the 3 least-significant digits. Add up each group in the pe# rmissions string, taking r=4, w=2, x=1. For example: 
#
#
# 421421421
# -rwxr-xr--
# \_/        -- r+w+x = 4+2+1 = 7
#    \_/     -- r+_+x = 4+0+1 = 5
#       \_/  -- r+_+_ = 4+0+0 = 4     => 0754 
#
# And finally, if you see modestring like so: 
# 
# -rwsr-xr-T
#
# The fourth digit is overloaded onto the x bits in the modestring. If you see a# letter other than x there, then it means one of these "special" fourth-digit b# its is set, and if the letter is lower case, then x for that position is also # set. So the translation for this one is:
#
#   4  2  1
# 421421421
# -rwsr-xr-T
#   +  +  +  -- s+_+T = 4+0+1 = 5  
#  \_/        -- r+w+s = 4+2+1 = 7  (s is lowercase, so 1)
#     \_/     -- r+_+x = 4+0+1 = 5
#        \_/  -- r+_+T = 4+0+0 = 4  (T is uppercase, so 0)   => 05754
#
# $!chkchng!.
#
# @sofreshitssuper sflabs.us
#
# sr.https://unix.stackexchange.com/questions/39710/how-to-get-permission-number# -by-string-rw-r-r
#

