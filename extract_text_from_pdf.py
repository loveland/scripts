import PyPDF2

# Path to the local PDF file
pdf_path = '/Users/mrfresh/Desktop/River & Sunny Custody/Leah/Leah Texts/Messages - Leah Grimason - Latest.pdf'

# Open the PDF file
with open(pdf_path, 'rb') as file:
    reader = PyPDF2.PdfReader(file)
    text = ''
    for page_num in range(len(reader.pages)):
        # Extract text from each page
        text += reader.pages[page_num].extract_text()

# Save the extracted text to a file
with open('extracted_text.txt', 'w') as text_file:
    text_file.write(text)

print("Text extracted and saved to 'extracted_text.txt'")

