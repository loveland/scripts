#!/bin/bash

# Define source and destination paths
SRC_PATH="/Volumes/ood/"
DEST_PATH="/Volumes/OOD_STORE/"

# Record the start time
START_TIME=$(date +%s)

# Run rsync with options for preserving permissions and handling interruptions
rsync -avz --partial --progress "$SRC_PATH" "$DEST_PATH"

# Calculate the duration
END_TIME=$(date +%s)
DURATION=$((END_TIME - START_TIME))

# Check if rsync encountered any errors
if [ $? -ne 0 ]; then
  # Email notification for errors
  {
    echo "Subject: Rsync Job Complete with Errors - smb://b230-304-t2/OOD_STORE (Old Share) to smb://137.78.104.33/ood (New Share)"
    echo "From: your_email@example.com"
    echo "To: your_email@example.com"
    echo
    echo "The rsync job from smb://b230-304-t2/OOD_STORE (Old Share) to smb://137.78.104.33/ood (New Share) encountered errors."
    echo "Duration: $DURATION seconds"
  } | /usr/sbin/sendmail -t
else
  # Email notification for successful completion
  {
    echo "Subject: Rsync Job Completed - smb://b230-304-t2/OOD_STORE (Old Share) to smb://137.78.104.33/ood (New Share)"
    echo "From: your_email@example.com"
    echo "To: your_email@example.com"
    echo
    echo "The rsync job from smb://b230-304-t2/OOD_STORE (Old Share) to smb://137.78.104.33/ood (New Share) has been completed successfully."
    echo "Duration: $DURATION seconds"
  } | /usr/sbin/sendmail -t
fi
