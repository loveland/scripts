#/bin/bash
#
#loveland

echo "DYL Backup and Restore #loveland" | lolcat
echo
echo "This utility will backup the most relevant files on employee mac workstations which includes:"
echo
echo "Backed up locations:"
echo "--------------------"
echo "~/Desktop"
echo "~/Documents"
echo "~/Downloads"
echo "~/Music"
echo "~/Videos"
echo "~/Pictures"
echo "~/Dropbox"
echo "& Mac Notes"
echo
echo "Upon running this application again, this program will then determine if there is your backup file on a new desktop and then"
echo "extract the contents from your backup into the same locations following a system migration for convienience."
echo
echo
sleep 1

localhostname=$(scutil --get LocalHostName)
filename="$localhostname"_bkup.zip
actualfile=$(find $HOME/Desktop -maxdepth 1 -name "*_bkup.zip" -print)

if [[ -z $actualfile ]] ; then

	echo "Backing up files to Desktop." | lolcat 
	brew install p7zip 
	brew install unzip
	7z a $HOME/Desktop/"$filename" $HOME/Desktop $HOME/Documents $HOME/Downloads $HOME/Music $HOME/Videos $HOME/Pictures $HOME/Dropbox ~/Library/Group\ Containers/group.com.apple.notes/ ~/Library/Containers/com.apple.Notes/Data/Library/Notes/

else

	actualfile=$(find $HOME/Desktop -maxdepth 1 -name "*_bkup.zip" -print)
	dirname=$(echo $actualfile | rev | cut -c 5- | rev)

	echo "Restoring files from backup..." | lolcat 

	cd ~/Desktop
	
	unzip $actualfile -d $dirname

	cd $dirname

	cp -rpf Desktop/* ~/Desktop
	cp -rpf Documents/* ~/Documents
   cp -rpf Downloads/* ~/Downloads
	cp -rpf Music/* ~/Music
	cp -rpf Videos/* ~/Videos
	cp -rpf Pictures/* ~/Pictures
	cp -rpf Dropbox ~/Dropbox
	cp -rpf group.com.apple.notes ~/Library/Group\ Containers/group.com.apple.notes/
	cp -rpf Notes ~/Library/Containers/com.apple.Notes/Data/Library/Notes/

	cd ~/Desktop

	rm -rf $dirname
	rm -rf $actualfile

	echo "Complete! Files Restored to Original Locations"
	echo
fi

echo "Press any key to close this window and exit the terminal or type exit to exit to shell:" | lolcat

read -p "" exitt

if [ "$exitt" == "exit" ]; then
	
	exit 1;

elif [ "$exitt" != "exit" ]; then

	clear
	echo "Buh Bye!"
	sleep 1
	superexit=$(ps ax | awk '! /awk/ && /Terminal/ { print $1}')
	kill -9 $superexit

fi
