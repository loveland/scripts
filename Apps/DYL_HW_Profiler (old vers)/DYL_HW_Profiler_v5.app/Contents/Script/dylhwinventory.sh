#!/bin/zsh
#
# Loveland
#
# This little script ensures proper net id, collects macosx name, hw model, serial #'s and mac addresses for DYL inventory
# purposes.
#
#
# Tested working as intended - Designed for MacOSX Catalina
#

sqldb="inventory"
sqltbl="client_inventory"
mysqlhost="165.22.134.9"
mysqluser="scripter"
mysqlpw="tB&BBniyz(76.J=6%-]A7eA"
echo

#NAME
	if [[ -z $(mysql -h$mysqlhost -u$mysqluser -p$mysqlpw -e "$SQL_EXISTS" $sqldb) ]]; then
		ID=$(mysql -h$mysqlhost -u$mysqluser -p$mysqlpw -D $sqldb -se "SELECT COUNT(*) FROM client_inventory;")
		ID="$(($ID+1))"
	else
		ID=1
	fi

AssignedTo=$(dscl . -read "/Users/$(who am i | awk '{print $1}')" RealName | sed -n 's/^ //g;2p')
ModifiedDate=$(date +"%Y-%m-%d %H:%M:%S")

	#--extra--
	FirstName=$(echo $AssignedTo | cut -d' ' -f1)
	LastName=$(echo $AssignedTo | cut -d' ' -f2)
	firstinitial=$(echo $AssignedTo | cut -c -1)
	IFS=" " read name lastinitial <<< $AssignedTo
	lastinitial=$(echo $lastinitial | cut -c -1)
	initials=$firstinitial$lastinitial

#HWID
SerialNumber=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Serial Number (system):' | cut -c 31-)
HardwareUUID=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Hardware UUID:' | cut -c 22-)
LANMAC=$(networksetup -getmacaddress en0 | cut -c 19- | cut -c -17)
WIFIMAC=$(networksetup -getmacaddress en1 | cut -c 19- | cut -c -17)

#HWPERF
RAM=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Memory:' | cut -c 15-)
TotalDiskSpace=$(diskutil info /dev/disk1s1 | grep "Container Total Space:" | cut -c31- | cut -c -9)
DiskSpaceRemaining=$(diskutil info /dev/disk1s1 | grep "Container Free Space:" | cut -c31- | cut -c -9) 
ProcessorName=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Processor Name:' | cut -c 23-)
ProcessorSpeed=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Processor Speed:' | cut -c 24-)
ProcessorCount=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Number of Processors:' | cut -c 29-) 
Cores=$(sysctl -n hw.ncpu)
L2CachePerCore=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'L2 Cache (per Core):' | cut -c 28-)
L3Cache=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'L3 Cache:' | cut -c 17-)

#OS
Hostname=$initials-$SerialNumber.dyl.com

OS=$(sw_vers | grep OS | cut -c13- | cut -c2-)
	if [[ $OS="Mac OS X" ]]; then
		Brand="Apple"
	fi

OSVersion=$(defaults read loginwindow SystemVersionStampAsString)
SysModel=$(sysctl hw.model | cut -c 11-)
LANIP=$(ifconfig en0 |awk '/inet / {print $2; }')
	if [[ $LANIP == "" ]]; then
		LANIP="N/A"
	fi
WIFIIP=$(ifconfig en1 |awk '/inet / {print $2; }')
	if [[ $WIFIIP == "" ]]; then
		WIFIIP="N/A"
	fi

	#--extra--
	LocalHostName=$FirstName$LastName
	NetBiosName=$firstinitial$LastName #can only have 15 chars

#KERNEL
BuildVersion=$(sw_vers | grep BuildVersion | cut -c 15-)
KernelVersion=$(uname -r)
BootRomVer=$(/usr/sbin/system_profiler SPHardwareDataType | grep 'Boot ROM Version:' | cut -c25-)
SMCVersion=$(system_profiler SPHardwareDataType | grep -i "SMC Version (system)" | awk -F ':' '{print $1 $2}' | cut -c28-)

sudo scutil --set ComputerName $LocalHostName
sudo scutil --set HostName $Hostname
sudo scutil --set LocalHostName $LocalHostName
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string $NetBiosName
dscacheutil -flushcache 

mysql -h$mysqlhost -u$mysqluser -p$mysqlpw -D $sqldb -e "CREATE DATABASE IF NOT EXISTS inventory; CREATE TABLE IF NOT EXISTS client_inventory (ID varchar (3), AssignedTo varchar(30), ModifiedDate varchar(21), Hostname varchar(30), Brand varchar(10), OS varchar(10), OSVersion varchar(10), SysModel varchar(20), LANIP varchar(15), WIFIIP varchar(15), SerialNumber varchar(20), HardwareUUID varchar(38), LANMAC varchar(20), WIFIMAC varchar(20), RAM varchar(5), TotalDiskSpace varchar(10), DiskSpaceRemaining varchar(8), ProcessorName varchar(30), ProcessorSpeed varchar(7), ProcessorCount varchar (1), Cores varchar (2), L2CachePerCore varchar (6), L3Cache varchar (6), BuildVersion varchar (8), BootRomVer varchar (50), KernelVersion varchar (6), SMCVersion varchar (30)) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
mysql -h$mysqlhost -u$mysqluser -p$mysqlpw -D $sqldb -e "REPLACE INTO client_inventory (ID, AssignedTo, ModifiedDate, Hostname, Brand, OS, OSVersion, SysModel, LANIP, WIFIIP, SerialNumber, HardwareUUID, LANMAC, WIFIMAC, RAM, TotalDiskSpace, DiskSpaceRemaining, ProcessorName, ProcessorSpeed, ProcessorCount, Cores, L2CachePerCore, L3Cache, BuildVersion, BootRomVer, KernelVersion, SMCVersion) VALUES ('$ID', '$AssignedTo', '$ModifiedDate', '$Hostname', '$Brand', '$OS', '$OSVersion', '$SysModel', '$LANIP', '$WIFIIP', '$SerialNumber', '$HardwareUUID', '$LANMAC', '$WIFIMAC', '$RAM', '$TotalDiskSpace', '$DiskSpaceRemaining', '$ProcessorName', '$ProcessorSpeed', '$ProcessorCount', '$Cores', '$L2CachePerCore', '$L3Cache', '$BuildVersion', '$BootRomVer', '$KernelVersion', '$SMCVersion');"

echo 
echo "INSERT INTO client_inventory (ID, AssignedTo, ModifiedDate, Hostname, Brand, OS, OSVersion, SysModel, LANIP, WIFIIP, SerialNumber, HardwareUUID, LANMAC, WIFIMAC, RAM, TotalDiskSpace, DiskSpaceRemaining, ProcessorName, ProcessorName, ProcessorSpeed, ProcessorCount, Cores, L2CachePerCore, L3Cache, BuildVersion, BootRomVer, KernelVersion, SMCVersion)" | lolcat
echo "VALUES"
echo "($ID, $AssignedTo, $ModifiedDate, $Hostname, $Brand, $OS, $OSVersion, $SysModel, $LANIP, $WIFIIP, $SerialNumber, $HardwareUUID, $LANMAC, $WIFIMAC, $RAM, $TotalDiskSpace, $DiskSpaceRemaining, $ProcessorName, $ProcessorName, $ProcessorSpeed, $ProcessorCount, $Cores, $L2CachePerCore, $L3Cache, $BuildVersion, $BootRomVer, $KernelVersion, $SMCVersion);"
echo

echo "Great Job. The $db database on $mysqlhost $sqltbl table has been updated." | lolcat; echo
