#!/bin/bash
# 
# Loveland.
#
# This little script will start and stop DirectMail campaigns using only known way how currently without cancelling them by simply starting
# stopping network services on a schedule.
#
#

PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
crondir="/usr/lib/cron/tabs/"
crondirinit="/etc/crontab"
clear

echo "*-------------------------------*"
echo "| DirectMail Campaign Scheduler |"
echo "*-------------------------------*"
echo ""
echo "This is for use to start and stop DirectMail Campaigns according to timezone."
echo "It will continue everyday at the same start and stop time until re-opened."
echo "Re-opening app will restore time zone to default if changed and also remove"
echo "any scheduling in place. Please enter your mac password below to provide access" 
echo "to system config needed to enable and disable. (Does not store password anywhere)"
echo ""
echo "-----------------------------------------------------------------------------"
echo ""

pw=$(sudo echo -n)

echo ""
echo "Refreshing Defaults for Regular System Use..."

timezone="America/Los_Angeles"
settimezone=$(sudo /usr/sbin/systemsetup -settimezone "$timezone" | sudo /usr/bin/killall SystemUIServer)
currenttimezone=$(sudo systemsetup -gettimezone | cut -c 12-)
crontab -r >/dev/null | echo "Any past scheduling cleared. Ready for new.."
networksetup -switchtolocation Automatic >/dev/null

echo ""
echo "*--------------------------------------*"
echo "|Current Time Zone: $currenttimezone|" 
echo "*--------------------------------------*"
echo ""
echo "Set New TimeZone For Upcoming Campaign Blast?" 
echo ""
printf "Options: EST, CMT, PST ('end' = Quit, Enter = Keep Same ): " & read -r timezone
echo ""

if [ "$timezone" == "PST" ]; then
	timezone="America/Los_Angeles"

elif [ "$timezone" == "CMT" ]; then
	timezone="America/Chicago"

elif [ "$timezone" == "EST" ]; then
	timezone="America/New_York"

elif [ -z "$timezone" ]; then
	timezone="$currenttimezone"

elif [ "$timezone" == "End" ] || [ "$timezone" == "end" ]; then
	osascript -e 'tell application "Terminal" to quit'; exit
fi

currenttimezone=$(sudo systemsetup -gettimezone | cut -c 12-)

if [ "$timezone" != "$currenttimezone" ]; then
	settimezone=$(sudo /usr/sbin/systemsetup -settimezone "$timezone" | sudo /usr/bin/killall SystemUIServer)
fi

echo "*--------------------------------------*"
echo "|Current Time Zone: $currenttimezone|" 
echo "*--------------------------------------*"
echo ""

echo "*--------*"
echo "|Campaign| "
echo "*--------*"
echo ""
printf "Start Time (24/hr eg. 09:00): " & read -r starttime

shour=$(echo $starttime | cut -c1-2)
smin=$(printf $starttime | tail -c 2 )

echo "Start Time= $shour:$smin"
echo ""
if [ "$smin" == "00" ]; then
	smin="*"
fi

printf "End @ Hour (24/hr eg. 17:00): " & read -r endhour

ehour=$(echo $endhour | cut -c1-2)
emin=$(printf $endhour | tail -c 2 )

echo "End Time= $ehour:$emin"
echo ""

if [ "$emin" == "00" ]; then
	emin="*"
fi

printf "Days of Week (all or weekdays=wd: " & read -r daysofweek
echo ""
echo "Email address to send start and paused job" 
printf "notifications to (press enter to keep local mail default): " & read -r email
echo ""

if [ ! -d "$crondirinit" ]; then
	sudo touch $crondirinit
fi

if [ -f $$crondir$(whoami) ]; then
	sudo cp $crondir$(whoami) $crondir$(whoami)_bkup
	sudo rm $crondir$(whoami)
	sudo touch $crondir$(whoami)
else
	sudo touch $crondir$(whoami)
fi

echo "PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" | sudo tee -a $crondir$(whoami)

if [ ! -z $email ]; then
	echo "MAILTO=$email" | sudo tee -a $crondir$(whoami)
fi

if [ $daysofweek == "all" ] || [ $daysofweek == "All" ]; then
	echo "$smin	$shour	*	*	*	networksetup -switchtolocation Automatic" | sudo tee -a $crondir$(whoami)
	echo "$emin	$ehour	*	*	*	networksetup -switchtolocation AirplaneMode" | sudo tee -a $crondir$(whoami)
else
	echo "$smin	$shour	*	*	1-5	networksetup -switchtolocation Automatic" | sudo tee -a $crondir$(whoami)
	echo "$emin	$ehour	*	*	1-5	networksetup -switchtolocation AirplaneMode" | sudo tee -a $crondir$(whoami)
fi

echo ""
echo "Done. Scheduled jobs in que. Exiting terminal in 3 seconds. (Re-running program will clear scheduled jobs to start new)"
echo ""   
sleep 3
osascript -e 'tell application "Terminal" to quit'; exit

#
# Legacy
#
# For historical purposes and possibly further use if ever needed, 
# here are other methods I used which will also work. 
#
#
# Method 1 (requires sudo)
#
# m1lup="sudo ifconfig en0 up && sudo ifconfig en0 up"
# m1ldn="sudo ifconfig en0 down && sudo ifconfig en0 down"
# m1wup="sudo ifconfig en0 up && sudo ifconfig en1 up"
# m1wdn="sudo ifconfig en0 up && sudo ifconfig en1 up"
#
# Method 2 (doesn't require sudo. No special profiles or otherwise needed further config to work. 4 lines. )
#
# m2lup="networksetup -setnetworkserviceenabled Ethernet on"
# m2ldn="networksetup -setnetworkserviceenabled Ethernet off"
# m2lup="networksetup -setnetworkserviceenabled WiFi on"
# m2lup="networksetup -setnetworkserviceenabled WiFi off"
#
# Method 3 (currently in use. must have preconfigured special 'AirPlane Mode' Profile created however. But less commands needed for execution [2])
#
#
# Other
#
# TWVHT=$(date +"%l:%M") # Displays AM PM format time using only single digit in hour and double digit in min
# TWFHT=$(date +"%R")    # Displays 24 Hour Time
#