import json
from ibm_watson import NaturalLanguageUnderstandingV1
from ibm_watson.natural_language_understanding_v1 import Features, SentimentOptions, EntitiesOptions, EmotionOptions
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

# Set up Watson NLU service
authenticator = IAMAuthenticator('hAMdzH13ArICr3MAY4Cgt4Klf0hsInHIleKmg2GlGRur')  # Replace with your IBM API key
nlu = NaturalLanguageUnderstandingV1(
    version='2021-08-01',
    authenticator=authenticator
)
nlu.set_service_url('https://api.us-south.natural-language-understanding.watson.cloud.ibm.com/instances/3e31756b-415a-4ac3-ab37-058c89631f40')  # Replace with your service URL

# Load the extracted text from the file
with open('extracted_text.txt', 'r') as text_file:
    text_data = text_file.read()

# Analyze the text
response = nlu.analyze(
    text=text_data,
    features=Features(
        sentiment=SentimentOptions(),
        entities=EntitiesOptions(emotion=True, sentiment=True),
        emotion=EmotionOptions()
    )
).get_result()

# Print the analysis results
print(json.dumps(response, indent=2))

