#!/usr/bin/perl

use strict;
use warnings;

use Expect;
use Data::Dumper;

my $package = $ARGV[0];

my $recur;
$recur = sub {
	my ($pkg, $retry) = @_;
	my $pkgdash = $pkg;
	$pkgdash =~ s/\:\:/-/g;
	print "*** Attempting to build: $pkg ($pkgdash) ***\n";
	my $specfile = "perl-${pkgdash}.spec";
	if (-e $specfile)
	{
		print "*** RPM Spec Found: $specfile for $pkg ***\n";
	}
	else
	{
		print "*** Running cpanspec for $pkg ***\n";
		my $ex = new Expect();
		$ex->raw_pty(1);
		my @cmd = ('cpanspec', '-v', $pkg);
		my $cmd = join(' ', @cmd);
		print "*** Command $cmd ***\n";
		unless ($ex->spawn(@cmd))
		{
			die "Cannot spawn $cmd: $!\n";
		}
		$ex->expect(undef);
	}
	unless (-e $specfile)
	{
		print "*** Unable to build RPM Spec for $pkg ***\n";
		return;
	}
	print "*** Attempting RPM Build with $specfile for $pkg ***\n";
	my $ex = new Expect();
	$ex->raw_pty(1);
	my @cmd = ('rpmbuild', '-ba', $specfile);
	my $cmd = join(' ', @cmd);
	print "*** Command $cmd ***\n";
	unless ($ex->spawn(@cmd))
	{
		die "Cannot spawn $cmd: $!\n";
	}
	my @deps = ();
	my @rpms = ();
	$ex->expect(undef,
		[ qr/\s+perl\(([\w\:]+)\) is needed by/, sub {
			my $exres = shift;
			my @ml = $exres->matchlist();
			push @deps, $ml[0];
			exp_continue;
		} ],
		[ qr/\s+perl\(([\w\:]+)\) \>\= [\d\.]+ is needed by/, sub {
			my $exres = shift;
			my @ml = $exres->matchlist();
			push @deps, $ml[0];
			exp_continue;
		} ],
		[ qr/Wrote\: \/root\/rpmbuild\/RPMS\/(.*?\.rpm)/, sub {
			my $exres = shift;
			my @ml = $exres->matchlist();
			push @rpms, $ml[0];
			exp_continue;
		} ],
		# Module Specific
		[ qr/These tests \*will\* \*fail\* if you do not have network connectivity. \[n\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/Check for a new version of the Public Suffix List\? \[N \]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/Do you want to install 'xml_pp' \(XML pretty printer\)\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("n\n");
			exp_continue;
		} ],
		[ qr/Do you want to install 'xml_grep' \(XML grep - grep XML files using XML::Twig's subset of XPath\)\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("n\n");
			exp_continue;
		} ],
		[ qr/Do you want to install 'xml_split' \(split big XML files\)\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("n\n");
			exp_continue;
		} ],
		[ qr/Do you want to install 'xml_merge' \(merge back files created by xml_split\)\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("n\n");
			exp_continue;
		} ],
		[ qr/Do you want to install 'xml_spellcheck' \(spellcheck XML files skipping tags\)\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("n\n");
			exp_continue;
		} ],
		[ qr/Do you want to build the XS Stash module\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/Do you want to use the XS Stash by default\? \[y\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/\[y\/N\] \[n\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/\[y\/N\] \[N\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/\[y\/N\] \[Y\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/\[N \]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/optional module\(s\) from CPAN\? \[n\]/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
		[ qr/Press return to continue/, sub {
			my $exres = shift;
			$exres->send("\n");
			exp_continue;
		} ],
	);
	if (scalar @rpms)
	{
		foreach my $r (@rpms)
		{
			print "*** Built RPM $r for $pkg ***\n";
			my $ex = new Expect();
			$ex->raw_pty(1);
			my @cmd = ('/bin/rpm', '-Uvh', "/root/rpmbuild/RPMS/$r");
			my $cmd = join(' ', @cmd);
			print "*** Installing RPM $r for $pkg ***\n";
			print "*** Command $cmd ***\n";
			unless ($ex->spawn(@cmd))
			{
				die "Cannot spawn $cmd: $!\n";
			}
			$ex->expect(undef,
				[ qr/\s+perl\(([\w\:]+)\) is needed by/, sub {
					my $exres = shift;
					my @ml = $exres->matchlist();
					push @deps, $ml[0];
					exp_continue;
				} ],
				[ qr/\s+perl\(([\w\:]+)\) \>\= [\d\.]+ is needed by/, sub {
					my $exres = shift;
					my @ml = $exres->matchlist();
					push @deps, $ml[0];
					exp_continue;
				} ],
			);
		}
	}
	if (scalar @deps)
	{
		foreach my $d (@deps)
		{
			print "*** Found dependency $d for $pkg ***\n";
		}
		foreach my $d (@deps)
		{
			$recur->($d);
		}
		unless ($retry)
		{
			$recur->($pkg, 1);
		}
	}
};

$recur->($package);