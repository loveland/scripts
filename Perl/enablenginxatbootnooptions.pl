use strict;
use warnings;

system("chkconfig", "nginx on");
if ( $? == -1 )
{
  print "command failed: $!\n";
}
else
{
  printf "\n nginx will be started at boot. \n (Command exited with value %d)\n\n", $? >> 8;
}
printf ""
