#!/usr/bin/perl

use warnings;
use strict;

#I've gotten into the habit of setting this on all my scripts, prevents weird path issues if the script is not being run by root
$ENV{'PATH'} = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin';

#Fill this with the perl modules required for your project
my @perl = qw(Log::Report XML::Compile XML::Compile::Cache XML::Compile::SOAP11);

chomp(my $curl = `which curl`);

if(!$curl){ system('apt-get install curl -y > /dev/null'); }

chomp(my $cpanm = system('/bin/bash', '-c', 'which cpanm &>/dev/null'));

#installs cpanm if missing
if($cpanm){ system('curl -s -L http://cpanmin.us | perl - --sudo App::cpanminus'); }

#loops through required modules and installs them if missing
foreach my $x (@perl){
    eval "use $x";
    if($@){
        system("cpanm $x");
        eval "use $x";
    }
}