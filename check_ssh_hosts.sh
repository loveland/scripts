#!/bin/bash
#
# This script accepts a copied and pasted list of hosts
# for the first input. Once pasted you need to press enter
# two times to get it to proceed. It will then check each host
# and print off a summary at the end of login results
# This is a first draft please feel free to make better as needed
# nlovelan
#

# Function to check if sshpass is installed
check_sshpass_installed() {
    if ! command -v sshpass &> /dev/null; then
        echo "sshpass is required but not installed. Please install it and run this script again."
        exit 1
    fi
}

# Check if sshpass is installed
check_sshpass_installed

# Prompt for the list of hostnames
echo "Enter the list of hostnames (one per line), followed by [ENTER]:"
hostnames=()
while IFS= read -r line; do
    [[ $line == '' ]] && break
    hostnames+=("$line")
done

# Prompt for the username and password
read -p "Enter the SSH username: " username
read -s -p "Enter the SSH password: " password
echo

# Arrays to store results
password_hosts=()
passcode_hosts=()
unresponsive_hosts=()
successful_hosts=()
hostname_error_hosts=()
password_failed_hosts=()

# Function to check host login method
check_host_login() {
    local host=$1
    echo "Checking host: $host"
    local result

    # Use expect to handle the interaction
    result=$(expect -c "
        set timeout 10
        spawn ssh -o ConnectTimeout=5 -o StrictHostKeyChecking=no $username@$host
        expect {
            \"*assword:\" {
                send \"$password\r\"
                expect {
                    \"*Permission denied*\" { exit 1 }
                    \"*Enter PASSCODE:*\" { exit 2 }
                    \"*Last login*\" { exit 0 }
                    timeout { exit 4 }
                }
            }
            \"*Enter PASSCODE:*\" { exit 2 }
            \"*Could not resolve hostname*\" { exit 5 }
            timeout { exit 3 }
            eof { exit 3 }
        }
    " 2>&1)

    case $? in
        0) successful_hosts+=("$host") ;;
        1) password_hosts+=("$host"); password_failed_hosts+=("$host") ;;
        2) passcode_hosts+=("$host") ;;
        3) unresponsive_hosts+=("$host") ;;
        4) unresponsive_hosts+=("$host") ;;
        5) hostname_error_hosts+=("$host") ;;
    esac
}

# Loop through the list of hostnames and check each one
for host in "${hostnames[@]}"; do
    check_host_login "$host"
done

# Output the results
echo -e "\nResults:"
echo
echo "Hosts that asked for a password but login failed:"
printf "   %s\n" "${password_failed_hosts[@]}"
echo
echo "Hosts that asked for 'Enter PASSCODE:':"
printf "   %s\n" "${passcode_hosts[@]}"
echo
echo "Hosts that did not respond:"
printf "   %s\n" "${unresponsive_hosts[@]}"
echo
echo "Hosts with hostname resolution errors:"
printf "   %s\n" "${hostname_error_hosts[@]}"
echo
echo "Hosts successfully logged in with password:"
printf "   %s\n" "${successful_hosts[@]}"

