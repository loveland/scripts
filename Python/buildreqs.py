#!/usr/bin/python
import sys
import re
build_re = re.compile('BuildRequires:.*')
compare_re = re.compile('.*=.*')
def main():
    if (len(sys.argv) > 1):
        spec = open(sys.argv[1])
        for line in spec:
            if build_re.match(line):
                for token in line.rsplit(" "):
                    if build_re.match(token):
                        continue
                    if compare_re.match(token):
                        break
                    token = token.rstrip(" ,\n\r")
                    if len(token) > 0:
                        print(token)
 
 
if __name__ == "__main__":
    main()
