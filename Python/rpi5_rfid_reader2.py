import pigpio
from time import sleep

# Initialize the pigpio library and connect to pigpiod
pi = pigpio.pi()
if not pi.connected:
    print("Could not connect to pigpiod")
    exit()

# Test LED or GPIO output on pin 17 (change to your desired GPIO pin)
led_pin = 17
pi.set_mode(led_pin, pigpio.OUTPUT)

# Toggle LED on and off
print("Turning LED on")
pi.write(led_pin, 1)  # Turn LED on
sleep(2)

print("Turning LED off")
pi.write(led_pin, 0)  # Turn LED off

# Stop pigpio
pi.stop()

