import os

def remove_empty_folders(source_directory):
    # Walk through the directory in reverse order (bottom-up)
    for root, dirs, _ in os.walk(source_directory, topdown=False):
        for dir_name in dirs:
            dir_path = os.path.join(root, dir_name)
            # Remove directory if it's empty
            if not os.listdir(dir_path):
                os.rmdir(dir_path)
                print(f"Removed empty folder: '{dir_path}'")

if __name__ == "__main__":
    source_directory = input("Enter the path to the directory: ").strip()
    if os.path.exists(source_directory):
        remove_empty_folders(source_directory)
    else:
        print("The provided directory does not exist.")

