import os
import shutil
import re
from collections import defaultdict

def organize_bios_files(source_directory):
    # Normalize folder names by aggressively removing duplicates and variations
    folder_names = []
    for root, dirs, _ in os.walk(source_directory):
        for dir_name in dirs:
            folder_names.append(os.path.relpath(os.path.join(root, dir_name), source_directory))

    # Create a mapping of canonical folder names to original folder paths
    folder_mapping = defaultdict(list)
    for folder in folder_names:
        # Remove non-alphanumeric characters, make lowercase, replace multiple spaces/underscores/dashes with a single empty string
        canonical_name = re.sub(r'[\s\W_]+', '', folder).lower()
        folder_mapping[canonical_name].append(os.path.join(source_directory, folder))

    # Merge similar folders
    processed_folders = set()
    for canonical_name, folders in folder_mapping.items():
        if len(folders) > 1:
            # Use the first folder as the target for merging others
            target_folder = folders[0]
            processed_folders.add(target_folder)
            for source_folder in folders[1:]:
                if not os.path.exists(source_folder) or source_folder in processed_folders:
                    continue
                # Move contents from duplicate folders to the target folder
                for item in os.listdir(source_folder):
                    item_path = os.path.join(source_folder, item)
                    target_path = os.path.join(target_folder, item)
                    # Handle conflicts if the file/folder already exists
                    if os.path.exists(target_path):
                        if os.path.isfile(item_path):
                            # Rename the file to avoid conflict
                            base, ext = os.path.splitext(item)
                            counter = 1
                            new_item = f"{base}_{counter}{ext}"
                            while os.path.exists(os.path.join(target_folder, new_item)):
                                counter += 1
                                new_item = f"{base}_{counter}{ext}"
                            target_path = os.path.join(target_folder, new_item)
                    shutil.move(item_path, target_path)
                # Remove the now-empty source folder
                try:
                    os.rmdir(source_folder)
                    print(f"Merged '{source_folder}' into '{target_folder}'.")
                except FileNotFoundError:
                    print(f"Folder '{source_folder}' not found, skipping removal.")
                except OSError as e:
                    print(f"Could not remove folder '{source_folder}': {e}")

    # Create new folders for files that are not yet categorized
    uncategorized_folder = os.path.join(source_directory, "Uncategorized")
    if not os.path.exists(uncategorized_folder):
        os.makedirs(uncategorized_folder)

    # Iterate over files in the source directory and all nested subdirectories, and move them to the appropriate merged folder
    processed_files = set()
    for root, _, files in os.walk(source_directory):
        if root in processed_folders or root == uncategorized_folder:
            continue
        for file in files:
            file_path = os.path.join(root, file)
            # Skip if already processed
            if file_path in processed_files:
                continue
            processed_files.add(file_path)

            # Move the file to the root of the appropriate merged folder
            destination_folder = None
            for canonical_name, folder_paths in folder_mapping.items():
                if canonical_name in re.sub(r'[\s\W_]+', '', file).lower():
                    destination_folder = folder_paths[0]
                    break

            if destination_folder:
                target_path = os.path.join(destination_folder, file)
                # Handle conflicts if the file already exists
                if os.path.exists(target_path):
                    base, ext = os.path.splitext(file)
                    counter = 1
                    new_file = f"{base}_{counter}{ext}"
                    while os.path.exists(os.path.join(destination_folder, new_file)):
                        counter += 1
                        new_file = f"{base}_{counter}{ext}"
                    target_path = os.path.join(destination_folder, new_file)
                shutil.move(file_path, target_path)
                print(f"Moved '{file}' to '{os.path.basename(destination_folder)}' folder.")
            else:
                # Move to uncategorized folder if no matching folder is found
                target_path = os.path.join(uncategorized_folder, file)
                if os.path.exists(target_path):
                    base, ext = os.path.splitext(file)
                    counter = 1
                    new_file = f"{base}_{counter}{ext}"
                    while os.path.exists(os.path.join(uncategorized_folder, new_file)):
                        counter += 1
                        new_file = f"{base}_{counter}{ext}"
                    target_path = os.path.join(uncategorized_folder, new_file)
                shutil.move(file_path, target_path)
                print(f"Moved '{file}' to 'Uncategorized' folder.")

if __name__ == "__main__":
    source_directory = input("Enter the path to the directory containing the BIOS files: ").strip()
    if os.path.exists(source_directory):
        organize_bios_files(source_directory)
    else:
        print("The provided directory does not exist.")
