from scapy.all import ARP, Ether, srp
import socket
import nmap
import requests

def scan_network(network):
    print("Scanning network for active devices...")
    arp = ARP(pdst=network)
    ether = Ether(dst="ff:ff:ff:ff:ff:ff")
    packet = ether / arp
    result = srp(packet, timeout=2, verbose=0)[0]

    devices = []
    for _, received in result:
        devices.append({'ip': received.psrc, 'mac': received.hwsrc})
    return devices

def scan_ports(ip):
    print(f"Scanning open ports on {ip}...")
    scanner = nmap.PortScanner()
    ports_info = {}
    try:
        scanner.scan(ip, '1-1024', '-T4')
        for protocol in scanner[ip].all_protocols():
            ports = scanner[ip][protocol].keys()
            ports_info[protocol] = list(ports)
    except Exception as e:
        print(f"Port scan failed for {ip}: {e}")
    return ports_info

def get_vendor(mac):
    try:
        url = f"https://api.macvendors.com/{mac}"
        response = requests.get(url, timeout=2)
        if response.status_code == 200:
            return response.text
    except Exception as e:
        print(f"Failed to fetch vendor for MAC {mac}: {e}")
    return "Unknown Vendor"

def main():
    network = input("Enter the network range (e.g., 192.168.1.0/24): ").strip()
    devices = scan_network(network)
    
    if not devices:
        print("No active devices found on the network.")
        return
    
    print("\nDevices found:")
    for device in devices:
        vendor = get_vendor(device['mac'])
        print(f"\nIP Address: {device['ip']}")
        print(f"MAC Address: {device['mac']}")
        print(f"Vendor: {vendor}")

        ports_info = scan_ports(device['ip'])
        if ports_info:
            for protocol, ports in ports_info.items():
                print(f"Open {protocol.upper()} Ports: {', '.join(map(str, ports))}")
        else:
            print("No open ports found.")

if __name__ == "__main__":
    main()

