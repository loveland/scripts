import os
from bs4 import BeautifulSoup

# replace with your file paths
bookmarks_file_path = '/Users/mrfresh/Desktop/Safari Bookmarks.html'
links_to_remove_file_path = '/Users/mrfresh/Desktop/x.txt'

# read the links to remove
with open(links_to_remove_file_path, 'r') as f:
    links_to_remove = set(f.read().splitlines())

# read the bookmarks file
with open(bookmarks_file_path, 'r') as f:
    soup = BeautifulSoup(f, 'html.parser')

# find all links that are in the links to remove
removed_links = []
for a in soup.find_all('a'):
    if a.has_attr('href') and a['href'] in links_to_remove:
        # remove the link
        a.extract()
        removed_links.append(a['href'])

# write the modified HTML to a new file
new_bookmarks_file_path = os.path.splitext(bookmarks_file_path)[0] + '-edited.html'
with open(new_bookmarks_file_path, 'w') as f:
    f.write(str(soup))

# write the removed links to a new file
new_links_file_path = os.path.splitext(links_to_remove_file_path)[0] + '-removed.txt'
with open(new_links_file_path, 'w') as f:
    f.write('\n'.join(removed_links))

print(f"The file was saved as {new_bookmarks_file_path}")
print(f"The removed links were saved as {new_links_file_path}")
