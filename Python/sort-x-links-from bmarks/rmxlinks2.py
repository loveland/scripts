import os
from urllib.parse import urlparse
from bs4 import BeautifulSoup

# replace with your file paths
bookmarks_file_path = '/Users/mrfresh/Desktop/Safari Bookmarks.html'
links_to_remove_file_path = '/Users/mrfresh/Desktop/x.txt'

# read the links to remove
with open(links_to_remove_file_path, 'r') as f:
    links_to_remove = set(f.read().splitlines())

# read the bookmarks file
with open(bookmarks_file_path, 'r') as f:
    soup = BeautifulSoup(f, 'html.parser')

# find all links that are in the links to remove
removed_links = []
for a in soup.find_all('a'):
    if a.has_attr('href') and a['href'] in links_to_remove:
        # remove the link
        removed_links.append(a)
        a.extract()

# remove duplicates
removed_links = list(dict.fromkeys(removed_links))

# write the modified HTML to a new file
new_bookmarks_file_path = os.path.splitext(bookmarks_file_path)[0] + '-edited.html'
with open(new_bookmarks_file_path, 'w') as f:
    f.write(str(soup))

# write the removed links to a new file
new_links_file_path = os.path.splitext(links_to_remove_file_path)[0] + '-removed.html'
with open(new_links_file_path, 'w') as f:
    new_soup = BeautifulSoup('<html><body></body></html>', 'html.parser')
    links_by_domain = {}
    for link in removed_links:
        domain = urlparse(link['href']).netloc
        if domain not in links_by_domain:
            links_by_domain[domain] = []
        links_by_domain[domain].append(link)
    for domain, links in links_by_domain.items():
        new_soup.body.append(new_soup.new_tag('h1', string=domain))
        for link in links:
            new_soup.body.append(link)
            new_soup.body.append(new_soup.new_tag('br'))  # add a line break after each link
    f.write(new_soup.prettify())

print(f"The file was saved as {new_bookmarks_file_path}")
print(f"The removed links were saved as {new_links_file_path}")
